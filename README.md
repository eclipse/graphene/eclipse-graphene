<!---
.. ===============LICENSE_START=======================================================
.. Graphene CC-BY-4.0
.. ===================================================================================
.. Copyright (C) 2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
.. ===================================================================================
.. This Graphene documentation file is distributed by AT&T and Tech Mahindra
.. under the Creative Commons Attribution 4.0 International License (the "License");
.. you may not use this file except in compliance with the License.
.. You may obtain a copy of the License at
..
..      http://creativecommons.org/licenses/by/4.0
..
.. This file is distributed on an "AS IS" BASIS,
.. WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
.. See the License for the specific language governing permissions and
.. limitations under the License.
.. ===============LICENSE_END=========================================================
-->

# Eclipse Graphene Installation

This repository holds installation and deployment scripts for the Eclipse Graphene system.

Software versions:
* Kubernetes 1.30.5
* Kube CNI 1.2.0
* Kubernetes dashboard 2.6.1
* Calico 3.28.1
* Helm 3.12.3
* Ingress-Nginx Chart 4.7.1

Preconditions:
* Fresh Ubuntu 24.04 server, one big partition
* At least 20GB of disk space available recommended (/var/lib alone will require more than 10GB)
* Installation user created that belongs to group **sudo**, in this example the user is **ai4eu**
* /etc/hosts has exactly one entry for the FQHN pointing to the externel ipv4 interface
* optionally letsencrypt certificates installed

Clone this repo in the home folder of the installation user and then:

    cd eclipse-graphene/tools
    bash setup_k8s_helm.sh 
    cd $HOME
    # replace FQHN appropriately
    bash eclipse-graphene/AIO/setup_prereqs.sh k8s FQHN $USER generic | tee log_prereqs.txt
    cd eclipse-graphene/AIO/
    bash oneclick_deploy.sh | tee log_oneclick.txt

To complete the installation, these manual steps are necessary:
* login as admin into nexus and set 'allow redeploy' for the repo maven-releases
* login ad admin into Graphene and do the Site Admin roles and allow all catalogs for role MLP user

Some of those scripts might take several minutes to complete execution.
The last script should end with output showing the URLs to use, e.g. among others:

    Portal: https://(your FQHN):443


# AutoSync Tool

The AutoSync tool helps onboard and publish multiple pipelines to a Graphene instance quickly and hassle-free. There are three modes of operation: 1) `TUTORIALS` mode, 2) `DOWNLOAD` mode, and 3) `UPLOAD` mode. Before you can start using this tool, there are a few simple steps to complete its setup.

### Setup Steps

---

1. Create a user in Graphene using an `<email>` and `<username>`. And don't forget to provide the publisher rights to your user.
2. Once the user is created, you must create your `User Token`. We first need to get your account's API token to do this. To get this, perform the following steps:
   * Login to Graphene using your newly created user. Go to the account setting.
   * Go to the `API Token` section and copy it.
   * Then, the User Token is created by joining the username from Step 1 and `API Token` with a colon (:)
     * For example `` `<username>:<API Token>` ``
     * This user token will be needed to invoke the script later.
3. Once we have the `User Token`, open the terminal and navigate to the eclipse-graphene installation path.
   * The default path should be `/home/ai4eu/eclipse-graphene`.
   * Navigate to the `AIO` folder, Hint: in this folder, `graphene_env.sh` and `nexus_env.sh` files should already exist.
   * Now, create an additional file with the name `import_env.sh` , and paste the following line into it:
     * `export GRAPHENE_TOKEN=<User Token>`
     * Where the `<User Token>` from Step 2 has been pasted.
   * Then go to `AIO/auto-sync-solutions` folder in the terminal.

That's it. Your AutoSync Tool should be ready to use. You can launch the tool by typing the terminal command: `python perform_sync.py`. Ensure you have all the necessary Python libraries installed in your execution environment to run the tool. This should launch a Flask application, which can be by default accessed by navigating to the link: `localhost:5000` in your browser.

The homepage should look like this.

![AutoSync_HomePage.png](/uploads/b1839d81695bb622ecd7061dcd016af6/AutoSync_HomePage.png)

#### Libraries

---

pip install requests flask numpy python-dotenv pillow


* python3-flask
* python3-numpy
* python3-pillow
* python3-dotenv
* python3-requests

## TUTORIALS MODE

After the Graphene has been freshly installed, it is usually empty. You can import 7 tutorial pipelines to get started and see how they work. For this purpose, the `TUTORIALS` mode is helpful. Once you are on the tool's homepage, click the button under the `TUTORIALS` heading. This should open a new webpage, similar to this screenshot.

![ImportTutorials.png](/uploads/df3dbbd2f7da934554428e14569d8311/ImportTutorials.png)

Click the Import Tutorials button, and you will see a loading modal window while your request is being processed. Once the process is over, you should be able to see the status of each pipeline. For detailed information, you can look through the terminal logs.

## DOWNLOAD MODE

When you manage multiple Graphene instances containing different pipeline solutions, you can skip manually onboarding and publishing individual pipelines to different Graphene instances. This mode provides an interface to see pipelines available on a source Graphene system and export them to a zip file. Once you are on the tool's homepage, click the button under the `DOWNLOAD` heading. This should open a new webpage, similar to this screenshot.

![DownloadSolutions.png](/uploads/bf12c1db480a2e02282be68e1d4b29b0/DownloadSolutions.png)

Here, you will be provided with a list of all available solutions on a source Graphene instance. You can select as many pipelines as you want to export and click on `Download Pipelines` button. While the tool processes your request, you will be shown a loading window. Later, you will see the status of downloading the requested pipelines. Once this is done, the tool will create a zip file in the location `AIO/auto-sync-solutions/auto_export_solutions/downloaded_solutions.zip` under your Eclipse Graphene setup. Once the process is over, you should be able to see the status of each pipeline. For detailed information, you can look through the terminal logs. 

You can now copy this zip file to as many target Graphene instances as you want and run the script in the `UPLOAD` mode on these instances to automatically onboard and publish these exported pipelines.

## UPLOAD MODE

This zip file, created by the `DOWNLOAD` mode can then be moved to the target Graphene host's filesystem. The exported pipelines can be onboarded and published to the current Graphene system using this mode. Copy the zip file on the target Graphene instance and place it under `AIO/auto-sync-solutions/auto_export_solutions/downloaded_solutions.zip` your Eclipse Graphene setup. Once you are on the tool's homepage, click the button under the `UPLOAD` heading. This should open a new webpage, similar to this screenshot.

![ImportSolutions.png](/uploads/3d4c4d90a272f83b26f740daeb1a1b14/ImportSolutions.png)

Here, you click on `Import Solutions` button, and the tool will automatically extract the pipelines from the zip file and, later, onboard and publish them. While the tool processes your request, you will be shown a loading window. Once the process is over, you should be able to see the status of each pipeline. For detailed information, you can look through the terminal logs.
