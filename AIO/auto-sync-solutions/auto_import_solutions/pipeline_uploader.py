#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2024 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import datetime
import json
import os
import uuid
from typing import Dict

from auto_import_solutions.model_uploader import ModelUploader
from auto_import_solutions.solution_uploader import SolutionUploader
from auto_sync_common.model import Model, Pipeline, Solution
from auto_sync_common.utils import bordered


class PipelineUploader(SolutionUploader):
    """
    This class is responsible to onboard and publish a pipeline onto a graphene host.
    """

    def __init__(
            self,
            solutionId_modelName_map,
            env_vars: Dict
    ) -> None:
        SolutionUploader.__init__(self, solutionId_modelName_map, env_vars)
        self.model_uploader = ModelUploader(solutionId_modelName_map, self.env_vars)

    def validate_solution(self, solution: Solution) -> bool:
        """
        Check if the uploaded composite solution is validated by the design studio api.
        :param solution: is a pipeline, where we check if the uploaded pipeline is valid.
        :return: `True` if the pipeline is successfully validated, otherwise `False`.
        """
        validate_solution_api = f'dsce/dsce/solution/validateCompositeSolution'
        version_number, revision_id = self.get_latest_version_number_revision_id(solution.solution_id)
        params = {
            "userId": self.GRAPHENE_USERID,
            "solutionName": solution.name,
            "solutionId": solution.solution_id,
            "version": int(version_number.split(".")[0])
        }
        response = self.api_client.post_request(validate_solution_api, params=params)
        status_code = response.status_code
        result = False
        if status_code == 200:
            if str.capitalize(json.loads(response.text)['success']) == 'False':
                print(f"Model Validation Failed!")
            else:
                print(f"Model Validation Succeeded!")
                result = True
            print(f"Response received: {response.text}")
        return result

    def create_new_composite_solution(self) -> object:
        """
        Creates a new composite solution, which is equivalent to initializing a blank canvas on design studio.
        :returns: the composite solution id (str) if successful, otherwise returns `False`.
        """
        create_comp_sol_api = "dsce/dsce/solution/createNewCompositeSolution"
        params = {
            "userId": self.GRAPHENE_USERID
        }
        response = self.api_client.post_request(create_comp_sol_api, params=params)
        status_code = response.status_code
        if status_code == 200:
            return json.loads(response.text)['cid']
        return False

    def add_pipeline_node(self, model_node: Model, cid: str):
        """
        This method adds a models as a node to the pipeline(composite solution).
        :param model_node: holds required details of the model to be added as a node.
        :param cid: holds the composite solution id of the pipeline.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        add_node_api = "dsce/dsce/solution/addNode"  # POST
        params = {
            "userId": self.GRAPHENE_USERID,
            "cid": cid
        }
        result = False
        add_node_payload_str = json.dumps(model_node.add_node_payload)
        solution_id = model_node.solution_id
        node_solution_id = model_node.nodeSolutionId
        node_id = model_node.nodeId
        name = model_node.name
        try:
            add_node_payload_str = add_node_payload_str.replace(node_id, name + "1")
            add_node_payload_str = add_node_payload_str.replace(node_solution_id, solution_id)

            response = self.api_client.post_request(add_node_api, data_json=json.loads(add_node_payload_str),
                                                    params=params)
            if response.status_code == 200:
                if str.capitalize(json.loads(response.text)['success']) == 'False':
                    print(f"Adding a new node to the pipeline failed!")
                else:
                    print(f"Adding a new node to the pipeline succeeded!")
                    result = True
                print(f"Response received: {response.text}")
        except TypeError as e:
            exception_message = f"Invalid {model_node.name} Model: Missing Uploaded Solution ID. Was the model uploaded successfully?"
            print(exception_message)
            new_error = TypeError(exception_message)
            return False, new_error
        return result

    def add_pipeline_link(self, pipeline: Pipeline, params: Dict):
        """
        This method adds a link between models in the pipeline(composite solution).
        :param pipeline: holds required details of the pipeline to be added as a node.
        :param params: a dictionary that holds the json based payload for adding link between two nodes in the pipeline.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        add_link_api = pipeline.solution["add_link_url"]
        link_id = str(uuid.uuid1())
        pipeline.link_ids.append(link_id)
        params['userId'] = self.GRAPHENE_USERID
        params['cid'] = pipeline.cid
        params['linkId'] = link_id
        for model_key in pipeline.models:
            model = pipeline.models[model_key]
            solution_id = model.solution_id
            node_solution_id = model.nodeSolutionId
            node_id = model.nodeId
            name = model.name
            add_link_api = add_link_api.replace(node_solution_id, solution_id)
            add_link_api = add_link_api.replace(node_id, name + "1")
            if model_key in params["sourceNodeId"]:
                if '/data/shared' not in params["sourceNodeName"]:
                    params["sourceNodeName"] = name + "1"
                params["sourceNodeId"] = name + "1"
            if model_key in params["targetNodeId"]:
                if '/data/shared' not in params["targetNodeName"]:
                    params["targetNodeName"] = name + "1"
                params["targetNodeId"] = name + "1"

        add_link_api = add_link_api.replace("{GRAPHENE_USER_ID}", self.GRAPHENE_USERID)
        add_link_api = add_link_api.replace("{COMP_SOLUTION_ID}", pipeline.cid)
        add_link_api = add_link_api.replace("{LINK_ID}", link_id)
        add_link_api = "dsce/dsce/solution/addLink"

        add_link_response = self.api_client.post_request(add_link_api, data_json={}, params=params)

        return add_link_response

    def modify_node(self, pipeline: Pipeline, modify_node_api: str):
        """
        This method modifies a node(model) in the pipeline(composite solution) on the design studio.
        :param pipeline: holds required details of the pipeline to be added as a node.
        :param modify_node_api: url to post the modification of the node.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        for model_key in pipeline.models:
            model = pipeline.models[model_key]
            solution_id = model.solution_id
            node_solution_id = model.nodeSolutionId
            node_id = model.nodeId
            name = model.name
            modify_node_api = modify_node_api.replace(node_solution_id, solution_id)
            modify_node_api = modify_node_api.replace(node_id, name + "1")
        modify_node_api = modify_node_api.replace("{GRAPHENE_USER_ID}", self.GRAPHENE_USERID)
        modify_node_api = modify_node_api.replace("{COMP_SOLUTION_ID}", pipeline.cid)
        modify_node_response = self.api_client.post_request(modify_node_api, None)
        return modify_node_response

    def save_composite_solution(self, pipeline: Pipeline):
        """
        This method adds a models as a node to the pipeline(composite solution).
        :param pipeline: holds required details of the pipeline being saved as a composite solution.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        save_comp_sol_api = ("dsce/dsce/solution/saveCompositeSolution?userId={GRAPHENE_USER_ID}&solutionName={"
                             "PIPELINE_NAME}&version=1&description=null&ignoreLesserVersionConflictFlag=false&cid={"
                             "COMP_SOLUTION_ID}")
        save_comp_sol_api = save_comp_sol_api.replace("{GRAPHENE_USER_ID}", self.GRAPHENE_USERID)
        save_comp_sol_api = save_comp_sol_api.replace("{COMP_SOLUTION_ID}", pipeline.cid)
        save_comp_sol_api = save_comp_sol_api.replace("{PIPELINE_NAME}", pipeline.name)
        save_comp_sol_response = self.api_client.post_request(save_comp_sol_api)
        return save_comp_sol_response

    def get_cdump_blueprint(self, pipeline: Pipeline):
        """
        This method downloads cdump.json and blueprint.json of the published the pipeline (composite solution).
        :param pipeline: holds required details of the pipeline.
        :return: boolean value, where `True` indicates the model was published successfully.
        """
        get_rev_artifact_api = "ccds/revision/{REVISION_ID}/artifact"
        get_rev_artifact_api = get_rev_artifact_api.replace("{REVISION_ID}", pipeline.revision_id)
        comp_sol_resp = self.api_client.get_request(get_rev_artifact_api)
        blueprint_artifact_uri = None
        blueprint_artifact = None
        cdump_artifact_uri = None
        cdump_artifact = None
        for element in comp_sol_resp:
            if element['artifactTypeCode'] == "BP":
                blueprint_artifact_uri = element["uri"]
                blueprint_artifact = self.document_mgr.get_document(blueprint_artifact_uri)
            if element['artifactTypeCode'] == "CD":
                cdump_artifact_uri = element["uri"]
                cdump_artifact = self.document_mgr.get_document(cdump_artifact_uri)
        pipeline.cdump = json.loads(cdump_artifact['content'].decode('utf-8'))
        pipeline.blueprint = json.loads(blueprint_artifact['content'].decode('utf-8'))
        save_result = pipeline.save_onboarding_files()
        return (blueprint_artifact, cdump_artifact)

    def upload_all_models(self, pipeline: Pipeline) -> Dict:
        """
        Onboards and publishes all the models of the provided pipeline to the target graphene host.
        :param pipeline: the pipeline objects with all its child models.
        :return: a dictionary where the keys are the names of the respective child models, and values are their publication status.
        """
        models = pipeline.models
        model_responses = {}
        for model_key in models.keys():
            # time.sleep(5)
            current_model = models[model_key]
            model_name = current_model.name
            print(bordered(f'MODEL: {model_name}'))
            model_details = None
            solution_exists, solution_details = self.check_if_solution_exists(model_name)
            if solution_exists:
                print(
                    f"The model with name `{model_name}` already exists!, skipping uploading the current model! Using the retrieved solution details!")
                model_details = solution_details
                current_model.solution_id = model_details['solutionId']
                current_model.revision_id = self.get_revision_id(current_model.solution_id)
            else:
                pub_resp = self.model_uploader.publish_model_approval_request(current_model)
                model_details = pub_resp
                print(f'Model published: {model_name, model_details}')
            model_responses[model_key] = model_details
        return model_responses

    def host_license_nexus(self, solution: Solution) -> bool:
        """
        This method uploads all the documents associated with a solution to nexus maven repository.
        It is one of the pre-publication step.
        :param solution: solution object, for which we need to upload the documents.
        :returns: bool value, where `True` means upload was successful, `False` otherwise
        """
        default_version = '1.0.0'
        solution_id = solution.solution_id
        license_doc_link = self.document_mgr.upload_document(solution_id, "license.json", solution.license_fp,
                                                             default_version)
        solution.solution["license.json"] = {
            'doc_link': license_doc_link,
            'doc_location': solution.license_fp,
            'doc_size': os.path.getsize(solution.license_fp)}
        return True

    def create_license_artifact_graphene(self, solution: Solution) -> bool:
        """
        This method stores links to the documents stored on nexus, which are associated with a solution.
        It is one of the pre-publication step.
        :param solution: solution object, for which we need to upload the documents.
        :returns: bool value, `True` meaning task was success, otherwise `False`
        """
        api_url = 'ccds/artifact'
        artifact_id = str(uuid.uuid1())
        solution.solution['artifact_id'] = artifact_id
        now = datetime.datetime.now()
        payload = {
            "artifactTypeCode": "LI",
            # "artifactId":artifact_id,
            "description": "license.json",
            "name": "license.json",
            # "created":now.strftime('%Y-%m-%dT%H:%M:%SZ'),
            # "modified":now.strftime('%Y-%m-%dT%H:%M:%SZ'),
            "size": solution.solution["license.json"]['doc_size'],
            "uri": solution.solution["license.json"]['doc_link'],
            "userId": self.env_vars['GRAPHENE_USERID'],
            "version": "1.0.0",
            # "metadata": {
            #     "tag": "license.json"
            # }
        }
        response = self.api_client.post_request(api_url, data_json=payload)
        solution.solution['license.json']['artifact_id'] = response.json()["artifactId"]
        # Set the artifact id as solution.solution['license.json']['artifact_id']
        return True

    def link_license_artifact_to_solution(self,
                                          catalog_id: str,
                                          solution: Solution
                                          ) -> bool:
        """
        This method links the documents stored on graphene, with a revision id of a solution in a catalog.
        It is one of the pre-publication step.
        :param catalog_id: identifier of the catalog, to which the solution belongs to.
        :param solution: solution object, for which we need to upload the documents.
        :returns: bool value, `True` meaning task was success, otherwise `False`
        """
        revision_id = solution.revision_id
        artifact_id = solution.solution['license.json']['artifact_id']
        api_url = f"ccds/revision/{revision_id}/artifact/{artifact_id}"
        response = self.api_client.post_request(api_url, None)
        if response.status_code == 200:
            return True
        return False

    def upload_license(self, solution: Solution) -> bool:
        """
        This method has three steps:
        1. Stores license json on the nexus repository.
        2. Links the license stored on nexus, with artifact controller of graphene.
        3. Links the artifact stored on graphene, with a revision id of a solution in a catalog.
        It is one of the pre-publication step.
        :param solution: solution object, for which we need to upload the documents.
        :returns: bool value, `True` meaning task was success, otherwise `False`
        """
        hosting_status = self.host_license_nexus(solution)
        creating_status = self.create_license_artifact_graphene(solution)
        linking_status = self.link_license_artifact_to_solution(self.GRAPHENE_CATALOGID, solution)
        overall_status = hosting_status and creating_status and linking_status
        print(f'Upload license.json Status: {overall_status}')
        return overall_status
