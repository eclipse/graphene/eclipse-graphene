#!/bin/bash
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2017-2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T and Tech Mahindra
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================
#
# What this is: Script to set environment file for ELK stack depoyment for Graphene.
# Prerequisites:
# - Ubuntu Xenial or Centos 7 server
# - MariaDB and Graphene AIO installed, environment files in folder AIO_ROOT
# Usage
# - Intended to be called from deploy-elk.sh and other scripts in this repo
# - Customize the values here for your needs.
# - defaults are set by "${parameter}:-default}"

# Graphene project Registries
export GRAPHENE_PROJECT_NEXUS_USERNAME=docker
export GRAPHENE_PROJECT_NEXUS_PASSWORD=docker
# Should NOT need to use Snapshot
export GRAPHENE_SNAPSHOT=nexus3.graphene.org:10003
# Should ONLY use Staging, if Release version not available or compatible
export GRAPHENE_STAGING=nexus3.graphene.org:10004
# Should ONLY use Release version
export GRAPHENE_RELEASE=nexus3.graphene.org:10002

if [[ -e elk_env.sh ]]; then source elk_env.sh; fi

export GRAPHENE_ELK_NAMESPACE="${GRAPHENE_ELK_NAMESPACE:-graphene-elk}"
export GRAPHENE_ELK_DOMAIN="${GRAPHENE_ELK_DOMAIN:-$GRAPHENE_DOMAIN}"
export GRAPHENE_ELK_HOST="${GRAPHENE_ELK_HOST:-$GRAPHENE_HOST}"
export GRAPHENE_ELK_HOST_IP="${GRAPHENE_ELK_HOST_IP:-$GRAPHENE_HOST_IP}"

# External component options
export GRAPHENE_HTTP_PROXY="${GRAPHENE_HTTP_PROXY:-}"
export GRAPHENE_HTTPS_PROXY="${GRAPHENE_HTTPS_PROXY:-}"

# Set by setup_elk.sh
export DEPLOY_RESULT=
export FAIL_REASON=

# Component options
export GRAPHENE_ELK_ELASTICSEARCH_PORT="${GRAPHENE_ELK_ELASTICSEARCH_PORT:-30930}"
export GRAPHENE_ELK_ELASTICSEARCH_INDEX_PORT="${GRAPHENE_ELK_ELASTICSEARCH_INDEX_PORT:-30920}"
export GRAPHENE_ELK_LOGSTASH_PORT="${GRAPHENE_ELK_LOGSTASH_PORT:-30500}"
export GRAPHENE_ELK_KIBANA_PORT="${GRAPHENE_ELK_KIBANA_PORT:-30561}"
export GRAPHENE_ELK_ES_JAVA_HEAP_MIN_SIZE="${GRAPHENE_ELK_ES_JAVA_HEAP_MIN_SIZE:-2g}"
export GRAPHENE_ELK_ES_JAVA_HEAP_MAX_SIZE="${GRAPHENE_ELK_ES_JAVA_HEAP_MAX_SIZE:-2g}"
export GRAPHENE_ELK_LS_JAVA_HEAP_MIN_SIZE="${GRAPHENE_ELK_LS_JAVA_HEAP_MIN_SIZE:-1g}"
export GRAPHENE_ELK_LS_JAVA_HEAP_MAX_SIZE="${GRAPHENE_ELK_LS_JAVA_HEAP_MAX_SIZE:-2g}"

# Persistent Volume options
# TODO: Address Elasticsearch access to PV requiring privileged security context
export GRAPHENE_ELASTICSEARCH_PRIVILEGED_ENABLE="${GRAPHENE_ELASTICSEARCH_PRIVILEGED_ENABLE:-true}"
export GRAPHENE_ELASTICSEARCH_DATA_PVC_NAME="${GRAPHENE_ELASTICSEARCH_DATA_PVC_NAME:-elasticsearch-data}"
export GRAPHENE_ELASTICSEARCH_DATA_PV_NAME="${GRAPHENE_ELASTICSEARCH_DATA_PV_NAME:-elasticsearch-data}"
export GRAPHENE_ELASTICSEARCH_DATA_PV_SIZE="${GRAPHENE_ELASTICSEARCH_DATA_PV_SIZE:-10Gi}"
export GRAPHENE_ELASTICSEARCH_DATA_PV_CLASSNAME="${GRAPHENE_ELASTICSEARCH_DATA_PV_CLASSNAME:-$GRAPHENE_10GI_STORAGECLASSNAME}"

cat <<EOF >elk_env.sh
export GRAPHENE_ELK_NAMESPACE=$GRAPHENE_ELK_NAMESPACE
export GRAPHENE_ELK_DOMAIN=$GRAPHENE_ELK_DOMAIN
export GRAPHENE_ELK_HOST=$GRAPHENE_ELK_HOST
export GRAPHENE_ELK_HOST_IP=$GRAPHENE_ELK_HOST_IP
export GRAPHENE_HTTP_PROXY=$GRAPHENE_HTTP_PROXY
export GRAPHENE_HTTPS_PROXY=$GRAPHENE_HTTPS_PROXY
export GRAPHENE_ELK_ELASTICSEARCH_PORT=$GRAPHENE_ELK_ELASTICSEARCH_PORT
export GRAPHENE_ELK_ELASTICSEARCH_INDEX_PORT=$GRAPHENE_ELK_ELASTICSEARCH_INDEX_PORT
export GRAPHENE_ELK_LOGSTASH_PORT=$GRAPHENE_ELK_LOGSTASH_PORT
export GRAPHENE_ELK_KIBANA_PORT=$GRAPHENE_ELK_KIBANA_PORT
export GRAPHENE_ELK_ES_JAVA_HEAP_MIN_SIZE=$GRAPHENE_ELK_ES_JAVA_HEAP_MIN_SIZE
export GRAPHENE_ELK_ES_JAVA_HEAP_MAX_SIZE=$GRAPHENE_ELK_ES_JAVA_HEAP_MAX_SIZE
export GRAPHENE_ELK_LS_JAVA_HEAP_MIN_SIZE=$GRAPHENE_ELK_LS_JAVA_HEAP_MIN_SIZE
export GRAPHENE_ELK_LS_JAVA_HEAP_MAX_SIZE=$GRAPHENE_ELK_LS_JAVA_HEAP_MAX_SIZE
export GRAPHENE_ELASTICSEARCH_DATA_PVC_NAME=$GRAPHENE_ELASTICSEARCH_DATA_PVC_NAME
export GRAPHENE_ELASTICSEARCH_DATA_PV_NAME=$GRAPHENE_ELASTICSEARCH_DATA_PV_NAME
export GRAPHENE_ELASTICSEARCH_DATA_PV_SIZE=$GRAPHENE_ELASTICSEARCH_DATA_PV_SIZE
export GRAPHENE_ELASTICSEARCH_DATA_PV_CLASSNAME=$GRAPHENE_ELASTICSEARCH_DATA_PV_CLASSNAME
EOF
