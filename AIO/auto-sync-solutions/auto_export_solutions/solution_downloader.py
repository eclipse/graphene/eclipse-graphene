#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2024 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================
import json
import os
import shutil
from io import BytesIO
from typing import Dict, List

import numpy as np
from PIL import Image

from auto_sync_common.document_manager import DocumentManager
from auto_sync_common.model import Pipeline
from auto_sync_common.utils import APIClient, FileManager
from requests.exceptions import JSONDecodeError


class SolutionDownloader(object):
    """
    This class reads the list of solution ids belonging to either models or pipelines.
    And downloads all artifacts related to a solution.
    This class can be configured from a json file, and it downloads solutions from the specified host.
    It reads a lot of authentication related details from environment variables, make sure to set relevant details.
    The required environment variables are:
    - GRAPHENE_HOST
    - REGISTRY_HOST
    - GRAPHENE_TOKEN
    - GRAPHENE_USER
    - GRAPHENE_PW
    - GRAPHENE_CATALOGID
    - GRAPHENE_USERID
    """

    def __init__(
            self,
            env_config: Dict
    ) -> None:
        # setup parameters from reading environment variables.
        self.SRC_GRAPHENE_HOST = env_config['GRAPHENE_HOST']  # FQHN like aiexp-preprod.ai4europe.eu
        self.REGISTRY_HOST = env_config['REGISTRY_HOST']
        self.SRC_GRAPHENE_TOKEN = env_config['GRAPHENE_TOKEN']  # format is 'graphene_username:API_TOKEN'
        self.SRC_GRAPHENE_USER = env_config['GRAPHENE_USER']
        self.SRC_GRAPHENE_PW = env_config['GRAPHENE_PW']
        self.SRC_GRAPHENE_CATALOGID = env_config['GRAPHENE_CATALOGID']
        self.SRC_GRAPHENE_USERID = env_config['GRAPHENE_USERID']
        self.SRC_NEXUS_RW_USERNAME = env_config['NEXUS_RW_USERNAME']
        self.SRC_NEXUS_RW_PASSWORD = env_config['NEXUS_RW_PASSWORD']
        self.SRC_NEXUS_HOST = env_config['NEXUS_HOST']
        self.SRC_NEXUS_PORT = env_config['NEXUS_PORT']
        self.SRC_NEXUS_REPO_NAME = env_config['NEXUS_REPO_NAME']
        self.SRC_NEXUS_ROOT_GROUP_ID = env_config['NEXUS_ROOT_GROUP_ID']
        self.SRC_GRAPHENE_DOC_USERID = env_config['GRAPHENE_DOC_USERID']
        self.docker_base = f'{self.REGISTRY_HOST}:7444/ai4eu-experiments/openml'
        self.ret_dir_name = "retrieved"
        self.header_type = {
            "Content-Type": "application/json",
            "Authorization": self.SRC_GRAPHENE_TOKEN,
        }
        self.host = f'https://{self.SRC_GRAPHENE_HOST}'
        self.api_client = APIClient(self.SRC_GRAPHENE_USER, self.SRC_GRAPHENE_PW, self.host)
        self.document_mgr = DocumentManager(env_config)
        self.get_document_manager()

    def get_document_manager(self):
        doc_manager_details = {
            'NEXUS_RW_USERNAME': self.SRC_NEXUS_RW_USERNAME,
            'NEXUS_RW_PASSWORD': self.SRC_NEXUS_RW_PASSWORD,
            'NEXUS_HOST': self.SRC_NEXUS_HOST,
            'NEXUS_PORT': self.SRC_NEXUS_PORT,
            'NEXUS_REPO_NAME': self.SRC_NEXUS_REPO_NAME,
            'NEXUS_ROOT_GROUP_ID': self.SRC_NEXUS_ROOT_GROUP_ID
        }
        self.document_mgr.init_with_details(doc_manager_details)

    def get_solution_list(self) -> Dict:
        """
        Retrieves the list of solutions (model/pipeline) from the graphene host.
        :return: dictionary object with relevant details of all the solutions available on the source graphene host.
        """
        resp = self.api_client.get_request(f"ccds/solution")
        return resp

    def get_solution(self, solution_id: str) -> Dict:
        """
        Retrieves the solution (model/pipeline) from the source graphene host.
        :param solution_id: solution identifier to retrieve appropriate model/pipeline from the graphene host.
        :return: dictionary object with relevant details of the retrieved solution.
        """
        resp = self.api_client.get_request(f"ccds/solution/{solution_id}")
        return resp

    def get_revision_id(self, solution_id: str):
        """
        This method calls an api to get the latest revision id of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :return: the latest revision number for the onboarded solution.
        """
        revision_api = f'ccds/solution/{solution_id}/revision'
        response = self.api_client.get_request(revision_api)
        if response:
            version = np.array([i['version'] for i in response]).argmax()
            return response[version]['revisionId']
        else:
            print(f'Can not get Revision ID. Error code: {response.status_code}')
            return 0

    def get_solution_artifacts(self, revision_id) -> Dict:
        """
        This method fetches all the artifacts that belong to a particular revision of a solution.
        :param revision_id: a string representing the revision id of a solution hosted on graphene.
        :return: a dictionary representing the artifacts of a model.
        """
        artifacts_api = f'ccds/revision/{revision_id}/artifact'
        response = self.api_client.get_request(artifacts_api)
        return response

    def get_authors_publisher(self, solution_id: str, revision_id: str) -> Dict:
        """
        This method calls an api to get the author and publisher details of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the author and publisher details.
        :return: dictionary value, where it represents the author details of a particular graphene solution.
        """
        author_api = f'ccds/solution/{solution_id}/revision/{revision_id}'
        response = self.api_client.get_request(author_api)
        authors = None
        publisher = None

        if "authors" in response:
            authors = response["authors"]
        if "publisher" in response:
            publisher = response["publisher"]
        print(f'Authors and Publisher: \n{authors}\n{publisher}')
        return {'authors': authors, "publisher": publisher}

    def get_description(self, revision_id: str, catalog_id: str) -> Dict:
        """
        This method calls an api to get the solution description of the solution.
        :param revision_id: current revision number for which we need to get the description.
        :param catalog_id: catalog identifier in which the solution is available.
        :return: string that represents the solution's description.
        """
        desc_api = f'ccds/revision/{revision_id}/catalog/{catalog_id}/descr'
        try:
            response = self.api_client.get_request(desc_api)
        except JSONDecodeError as e:
            return {"description": "No Description Found!"}
        description = None
        if "description" in response:
            description = response["description"]
        print(f'Setup Description Status: {description}')
        return {"description": description}

    def get_icon(self, solution_id: str) -> bytes:
        """
        This method calls an api to get the icon of the solution.
        :param solution_id: solution identifier for the solution.
        :return: bytes string that represent the solution's icon.
        """
        icon_api = f'ccds/solution/{solution_id}/pic'
        icon = self.api_client.get_image(icon_api)
        return icon

    def store_icon(self, icon: bytes, folder_location: str):
        """
        Store the icon available in bytes format to the provided directory.
        :param icon: the bytes string representation of the solution's icon. The icon will be stored either as `icon.png` or `icon.jpg` depending upon the file retrieved from server.
        :param folder_location: location of the solution folder
        Reference: https://stackoverflow.com/a/23489503
        """
        file_name = ''
        if icon:
            if "PNG" in str(icon):
                file_name = "icon.png"
            else:
                file_name = "icon.jpg"
            icon_img = Image.open(BytesIO(icon))
            icon_img.save(os.path.join(folder_location, file_name))
        return file_name

    def create_retrieval_dir(self, delete_old_files=False):
        """
        Creates a directory to store all the retrieved solutions.
        :param delete_old_files: a flag, when set True, deletes all previous solutions in the retreival directory.
        """
        if os.path.exists(self.ret_dir_name):
            if delete_old_files:
                shutil.rmtree(self.ret_dir_name)
            else:
                print("Not deleting old content")
        os.makedirs(self.ret_dir_name, exist_ok=True)

    def cdump_version_check(self, cdump: Dict) -> Dict:
        """
        This method checks the version for all nodes in the cdump to be 1.0.0.
        If version for a node in the cdump is different, then we reset it to 1.0.0.
        This is required, as a new model to be onboarded by script is always the first version.
        :param cdump: the cdump file to be checked.
        :return: the cdump with all rectified versions of node.
        """
        for node in cdump['nodes']:
            if node['nodeVersion'] != '1.0.0':
                node['nodeVersion'] = '1.0.0'
        return cdump

    def get_cdump_blueprint(self, pipeline_revision_id: Pipeline, pipeline_location):
        """
        This method downloads cdump.json and blueprint.json of the published the pipeline (composite solution).
        :param pipeline: holds required details of the pipeline.
        :return: boolean value, where `True` indicates the model was published successfully.
        """
        get_rev_artifact_api = "ccds/revision/{REVISION_ID}/artifact"
        get_rev_artifact_api = get_rev_artifact_api.replace("{REVISION_ID}", pipeline_revision_id)
        comp_sol_resp = self.api_client.get_request(get_rev_artifact_api)
        blueprint_artifact_uri = None
        blueprint_artifact = None
        cdump_artifact_uri = None
        cdump_artifact = None
        save_blueprint = False
        save_cdump = False
        for element in comp_sol_resp:
            if element['artifactTypeCode'] == "BP":
                blueprint_artifact_uri = element["uri"]
                blueprint_artifact = self.document_mgr.get_document(blueprint_artifact_uri)
                with open(os.path.join(pipeline_location, 'blueprint.json'), 'w') as bp_file:
                    json.dump(json.loads(blueprint_artifact['content'].decode("utf-8")), bp_file)
                save_blueprint = True
            if element['artifactTypeCode'] == "CD":
                cdump_artifact_uri = element["uri"]
                cdump_artifact = self.document_mgr.get_document(cdump_artifact_uri)
                with open(os.path.join(pipeline_location, 'cdump.json'), 'w') as cd_file:
                    cdump_dict = json.loads(cdump_artifact['content'].decode("utf-8"))
                    rectified_cdump_dict = self.cdump_version_check(cdump_dict)
                    json.dump(rectified_cdump_dict, cd_file)
                save_cdump = True
        save_result = save_cdump and save_blueprint
        return save_result

    def get_child_nodes_of_pipeline(self, pipeline_solution_id):
        """
        This method gives a list of solution ids, that are part of the pipeline.
        :param pipeline_solution_id: solution id of the pipeline.
        :return: list of solutions ids, of all the member nodes of pipeline.
        """
        child_solution_ids = []
        api_link = f"ccds/solution/{pipeline_solution_id}/comp"
        child_solution_ids = self.api_client.get_request(api_link)
        return child_solution_ids

    def get_catalog_id(self, solution_id):
        """
        Fetches the correct catalog id for the provided solution_id
        :param solution_id: solution_id of the

        """
        api = f"/api/catalog/solution/{solution_id}"
        response = self.api_client.get_request(api)
        if response['response_body']:
            return response['response_body'][0]['catalogId']
        else:
            return None

    def prepare_folder_structure_model(self):
        pass

    def download_solution(self, solution_id: str):
        pass

    def check_if_solution_active(self, solution_id: str) -> bool:
        """
        :param solution_id: solution identifier for the
        """
        is_active = False
        api = f'ccds/solution/{solution_id}'
        response = self.api_client.get_request(api)
        if 'active' in response and response['active']:
            is_active = True
        return is_active


def download_pipelines(env_config: Dict) -> List:
    """
    This function takes a list of solution ids, and downloads them as a zip.
    :param env_config: is a dictionary with all the necessary details of source graphene instance.
    :returns: list of downloaded  pipelines.
              Whenever a pipeline download fails, the list contains None on the respective list index.
    """
    from auto_export_solutions.pipeline_downloader import PipelineDownloader
    base_download_folder = 'auto_export_solutions/downloaded_solutions'
    abs_base_download_folder_path = os.path.abspath(base_download_folder)
    if os.path.exists(abs_base_download_folder_path):
        shutil.rmtree(abs_base_download_folder_path)
    os.makedirs(abs_base_download_folder_path)
    pipeline_downloader = PipelineDownloader(env_config)
    downloaded_pipelines = []
    for pipeline_solution_id in env_config['SOLUTION_IDS']:
        downloaded_pipelines.append(pipeline_downloader.download_solution(pipeline_solution_id))
        print('\n\n\n\n\n')
    downloaded_solutions = []
    for pipeline_tuple in downloaded_pipelines:
        pipeline_name, pipeline_object, pipeline_exception = pipeline_tuple
        if pipeline_object is not None:
            downloaded_solutions.append({"pipeline_name": f"{pipeline_object.base_solution_name}",
                                         "location": f"{pipeline_object.base_solution_name}"})
    downloaded_solutions_json_path = os.path.join(os.path.abspath('./auto_export_solutions/downloaded_solutions'),
                                                  'downloaded_solutions.json')
    with open(downloaded_solutions_json_path, 'w') as downloaded_solutions_fp:
        json.dump(downloaded_solutions, downloaded_solutions_fp)
    shutil.make_archive(base_download_folder, 'zip', 'auto_export_solutions/downloaded_solutions')
    if len(downloaded_pipelines) == 0 or None in downloaded_pipelines:
        print('There are some invalid downloaded Pipelines!')
    return downloaded_pipelines


if __name__ == "__main__":
    print("Downloaded Solution")
    download_pipelines()
