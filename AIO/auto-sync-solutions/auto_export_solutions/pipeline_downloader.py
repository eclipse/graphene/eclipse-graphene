#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2024 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================
import json
import os
from typing import Dict, List, Tuple

from auto_export_solutions.model_downloader import ModelDownloader
from auto_export_solutions.solution_downloader import SolutionDownloader
from auto_sync_common.model import Pipeline
from auto_sync_common.utils import FileManager, NotFoundError, bordered, NotActiveSolutionError


class PipelineDownloader(SolutionDownloader):
    """
    This class reads the list of solution ids belonging to either models or pipelines.
    And downloads all artifacts related to a solution.
    This class can be configured from a json file, and it downloads solutions from the specified host.
    It reads a lot of authentication related details from environment variables, make sure to set relevant details.
    The required environment variables are:
    - GRAPHENE_HOST
    - REGISTRY_HOST
    - GRAPHENE_TOKEN
    - GRAPHENE_USER
    - GRAPHENE_PW
    - GRAPHENE_CATALOGID
    - GRAPHENE_USERID
    """

    def __init__(
            self,
            env_config: Dict
    ) -> None:
        SolutionDownloader.__init__(self, env_config)
        self.model_downloader = ModelDownloader(env_config)

    def get_relations_from_cdump(self, pipeline_location: str) -> List:
        """
        Extracts all the relations of different nodes of a pipeline, and prepares a list from template.
        This template is used for connect various nodes of pipeline with each other.
        :param pipeline_location: the location of pipeline on the filesystem.
        """
        cdump_fp = os.path.join(pipeline_location, 'cdump.json')
        cdump = None
        with open(cdump_fp, 'r') as cdump_file:
            cdump = json.load(cdump_file)
        relations = []
        for relation in cdump['relations']:
            relation_template = {
                "userId": "{self.GRAPHENE_USERID}",
                "cid": "{pipeline.cid}",
                "linkId": "{link_id}",
                "sourceNodeName": relation["sourceNodeName"],
                "sourceNodeId": relation["sourceNodeId"],
                "targetNodeName": relation["targetNodeName"],
                "targetNodeId": relation["targetNodeId"],
                "sourceNodeRequirement": relation["sourceNodeRequirement"],
                "targetNodeCapabilityName": relation["targetNodeCapability"]
            }
            relations.append(relation_template)
        return relations

    def download_solution(self, solution_id: str) -> Tuple:
        """
        This method downloads the pipeline
        """
        pipeline_solution_id = solution_id
        pipeline_details = self.get_solution(solution_id=pipeline_solution_id)

        pipeline_revision_id = self.get_revision_id(pipeline_solution_id)
        pipeline_name = pipeline_details['name']
        print(bordered(f'PIPELINE: {pipeline_name}'))
        pipeline_artifacts = self.get_solution_artifacts(pipeline_revision_id)
        license_uri = None
        exception = None
        pipeline_object = None
        exception_message = None
        exception_messages = []
        try:
            if not self.check_if_solution_active(pipeline_solution_id):
                exception_message = f'Inactive Pipeline -- {pipeline_name} with solution id: {pipeline_solution_id}'
                print(exception_message)
                exception_messages.append(exception_message)
                e = NotActiveSolutionError(exception_message)
                raise e

            for artifact in pipeline_artifacts:
                if artifact['name'] == "license.json":
                    license_uri = artifact['uri']
                    break

            if license_uri is None:
                exception_message = f"Invalid {pipeline_name} Pipeline -- License File not found"
                print(exception_message)
                exception_messages.append(exception_message)
                exception = NotFoundError(exception_message)
                raise exception

            children_solution_ids = self.get_child_nodes_of_pipeline(pipeline_solution_id)
            curr_dir_location = os.path.dirname(__file__)
            download_folder_location = os.path.join(curr_dir_location, 'downloaded_solutions')
            pipeline_location = os.path.join(download_folder_location, pipeline_name)

            FileManager.create_directory(download_folder_location, pipeline_name)
            self.get_cdump_blueprint(pipeline_revision_id, pipeline_location)
            self.model_downloader.set_model_parent_dir(pipeline_location)
            children_models = [self.model_downloader.download_solution(model_solution_id) for model_solution_id in
                               children_solution_ids]
            has_exception_occured_false = False
            for child_tuple in children_models:
                child_name, child_model, child_exception = child_tuple
                if child_model is None:
                    has_exception_occured_false = True
                    exception_message = f"Invalid {pipeline_name} Pipeline - pipeline model {child_name} is missing essential artifacts! Skipping this pipeline!"
                    print(exception_message)
                    exception_messages.append(exception_message)
                    invalid_fp = os.path.join(pipeline_location, 'invalid_pipeline.json')
                    with open(invalid_fp, 'w') as invalid_pipeline_file:
                        json.dump({'invalid_pipeline': True}, invalid_pipeline_file)
            if has_exception_occured_false:
                exception = NotFoundError(exception_messages)
                raise exception

            license_json_artifact = self.document_mgr.get_document(license_uri)
            license_file_path = os.path.join(pipeline_location, "license.json")
            model_icon = self.get_icon(pipeline_solution_id)
            icon_file_name = self.store_icon(model_icon, pipeline_location)
            with open(license_file_path, 'wb') as license_file:
                license_file.write(license_json_artifact['content'])
            catalog_id = self.get_catalog_id(pipeline_solution_id)
            if catalog_id is None:
                exception_message = f"Invalid {pipeline_name} Pipeline - no catalogId was fetched!"
                print(exception_message)
                exception = NotFoundError(exception_message)
                exception_messages.append(exception_message)
                raise exception
            model_description = self.get_description(pipeline_revision_id, catalog_id)
            description_file_path = os.path.join(pipeline_location, "description.txt")
            with open(description_file_path, 'w') as description_file:
                description_file.write(model_description['description'])
            author_details = self.get_authors_publisher(pipeline_solution_id, pipeline_revision_id)
            # TODO: add_link_params to be fixed in the following json
            solutions_details = {
                "pipeline_name": pipeline_name,
                "location": pipeline_location,
                "models": [model[1].base_solution_name for model in children_models],
                "icon_filename": icon_file_name,
                'authors': author_details,
                "tags": {
                    "modelTypeCode": pipeline_details['modelTypeCode'],
                    'toolkitTypeCode': pipeline_details['toolkitTypeCode'],
                    'tags': pipeline_details['tags']
                },
                "documents": [],
                "add_link_params": self.get_relations_from_cdump(pipeline_location),
                "modify_node_urls": [

                ],
                "add_link_url": "dsce/dsce/solution/addLink"
            }

            for child_name, child_model, child_exception in children_models:
                print(child_model)
                if 'SharedFolderProvider' in child_model.name:
                    modify_link = "dsce/dsce/solution/modifyNode?userid={GRAPHENE_USER_ID}&cid={COMP_SOLUTION_ID}&"
                    modify_link += f"nodeid={child_model.solution['nodeId']}&nodename=/data/shared&ndata=" + '{"fixed":false}'
                    solutions_details['modify_node_urls'].append(modify_link)

            pipeline_object = Pipeline(solution_name=pipeline_name, solution_details=solutions_details,
                                       local_fpath=pipeline_location,
                                       is_retrieved=True)
            pipeline_solution_path = os.path.join(pipeline_location, 'solution.json')
            with open(pipeline_solution_path, 'w') as solution_json:
                json.dump(solutions_details, solution_json)
            pipeline_models = {child_model.name: child_model for child_name, child_model, child_exception in
                               children_models}
            pipeline_object.models = pipeline_models
            print(pipeline_object)
        except NotFoundError as e:
            return pipeline_name, pipeline_object, exception_messages
        except NotActiveSolutionError as e:
            return pipeline_name, pipeline_object, exception_messages
        return pipeline_name, pipeline_object, None
