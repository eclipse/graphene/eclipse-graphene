import json

from auto_sync_common.utils import APIClient


class PerformSync():
    def __init__(self, env_vars):
        self.env_vars = env_vars
        self.target_host = f'https://{self.env_vars["GRAPHENE_HOST"]}'

        self.target_api_client = APIClient(self.env_vars["GRAPHENE_USER"],
                                           self.env_vars["GRAPHENE_PW"],
                                           self.target_host)

    def get_list_catalogs(self):
        resp = self.target_api_client.get_request('ccds/catalog')
        return resp['content']

    def get_all_solutions_from_all_catalogs(self):
        pipeline_list = []
        catalog_list = [catalog['catalogId'] for catalog in self.get_list_catalogs()]
        api_link = 'ccds/catalog/solution?size=100000'
        for catalog_id in catalog_list:
            api_link += f'&ctlg={catalog_id}'
        resp = self.target_api_client.get_request(api_link)
        current_page_number = resp['number']
        total_pages = resp['totalPages']
        if total_pages > 0:
            while current_page_number < total_pages:
                api_link += f'&page={current_page_number}'
                resp = self.target_api_client.get_request(api_link)
                current_pipeline_list = [pipeline for pipeline in resp['content'] if
                                         pipeline['toolkitTypeCode'] == 'CP' and pipeline['active']]
                pipeline_list.extend(current_pipeline_list)
                current_page_number += 1
        pipeline_list = sorted(pipeline_list, key=lambda d: d['name'].lower())
        return pipeline_list

    def get_solutions_from_source(self):
        """
        Retrieves the list of solutions (model/pipeline) from the source graphene host.
        :return: dictionary object with relevant details of all the solutions available on the source graphene host.
        """
        resp = self.target_api_client.get_request(f"ccds/solution")
        return resp

    def get_pipelines_from_source(self):
        """
        Retrieves the list of solutions (model/pipeline) from the source graphene host.
        :return: dictionary object with relevant details of all the solutions available on the source graphene host.
        """
        solution_list = []
        response = self.target_api_client.get_request(f"ccds/solution?size=1000000")
        current_page_number = response['number']
        total_pages = response['totalPages']
        if total_pages > 1:
            while current_page_number < total_pages:
                response = self.target_api_client.get_request(f"ccds/solution?page={current_page_number}")
                for solution in response['content']:
                    if solution['active'] and solution['toolkitTypeCode'] == 'CP':
                        solution_list.append(solution)
                current_page_number += 1
        else:
            for solution in response['content']:
                if solution['active'] and solution['toolkitTypeCode'] == 'CP':
                    solution_list.append(solution)
        return solution_list

    def get_list_of_tutorial_pipelines(self):
        tutorials = {}
        with open('./auto_import_solutions/tutorial_solutions.json', 'r') as tutorials_file:
            tutorials = json.load(tutorials_file)
        tutorials_list = [" ".join(element['pipeline_name'].split('-')).title() for element in tutorials]
        return tutorials_list
