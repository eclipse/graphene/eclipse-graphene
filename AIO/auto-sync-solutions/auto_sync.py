#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2024 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import argparse
import os
import shutil
import zipfile
from glob import glob

from auto_export_solutions import solution_downloader
from auto_import_solutions import solution_uploader
from auto_sync_common.utils import EnvVarsManager

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Auto-Sync CLI Tool')
    parser.add_argument('-e', '--env-vars',
                        help='Root Directory where eclipse-graphene is installed. ' +
                             'This will help in locating the environment variables and uploading tutorials.' +
                             'Default value would be "/home/ai4eu/eclipse-graphene"',
                        required=False, default="/home/ai4eu/eclipse-graphene/AIO/")
    args = parser.parse_args()
    env_vars_mgr = EnvVarsManager(args.env_vars)
    target_env_vars = env_vars_mgr.target_env_config
    source_env_vars = env_vars_mgr.src_env_config
    IS_TUTORIAL_MODE = ('SYNC_MODE' in target_env_vars) and ("TUTORIALS" == target_env_vars["SYNC_MODE"])
    IS_DOWNLOAD_MODE = ('SYNC_MODE' in os.environ) and ("DOWNLOAD" == os.environ["SYNC_MODE"])
    IS_UPLOAD_MODE = ('SYNC_MODE' in os.environ) and ("UPLOAD" == os.environ["SYNC_MODE"])
    if IS_TUTORIAL_MODE:
        json_file_path = "./auto_import_solutions/tutorial_solutions.json"
        solution_uploader.upload_solutions(target_env_vars, json_file_path)
    if IS_DOWNLOAD_MODE:
        downloaded_pipelines = solution_downloader.download_pipelines(source_env_vars)
        if downloaded_pipelines is None or len(downloaded_pipelines) == 0:
            print(
                'All pipelines to be downloaded is invalid!'
                ' Please provide another source pipeline solution id and rerun this program!')
        else:
            if None in downloaded_pipelines:
                print('Some downloaded pipelines are invalid - they contained invalid or missing artifacts!')
    if IS_UPLOAD_MODE:
        json_file_path = './auto_import_solutions/downloaded_solutions/downloaded_solutions.json'
        with zipfile.ZipFile('auto_export_solutions/downloaded_solutions.zip', 'r') as zip_ref:
            zip_ref.extractall('auto_import_solutions/downloaded_solutions/')
        for element in glob('auto_import_solutions/downloaded_solutions/*'):
            if os.path.isdir(element):
                if 'invalid_pipeline.json' in os.listdir(element):
                    print(f"deleting {element}, invalid pipeline")
                    shutil.rmtree(element)
        downloaded_pipelines = glob('auto_import_solutions/downloaded_solutions/*/')
        solution_uploader.upload_solutions(target_env_vars, json_file_path)
