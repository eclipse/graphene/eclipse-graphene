#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2024 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================
import json
import os
from json import JSONDecodeError
from pathlib import Path
from typing import Dict, Tuple, Any

import requests

from auto_export_solutions.solution_downloader import SolutionDownloader
from auto_sync_common.model import Model
from auto_sync_common.utils import FileManager, NotFoundError, bordered, NotActiveSolutionError


class ModelDownloader(SolutionDownloader):
    """
    This class reads the list of solution ids belonging to either models or pipelines.
    And downloads all artifacts related to a solution.
    This class can be configured from a json file, and it downloads solutions from the specified host.
    It reads a lot of authentication related details from environment variables, make sure to set relevant details.
    The required environment variables are:
    - GRAPHENE_HOST
    - REGISTRY_HOST
    - GRAPHENE_TOKEN
    - GRAPHENE_USER
    - GRAPHENE_PW
    - GRAPHENE_CATALOGID
    - GRAPHENE_USERID
    """

    def __init__(
            self,
            env_config: Dict
    ) -> None:
        SolutionDownloader.__init__(self, env_config)
        self.model_parent_dir = Path()

    def set_model_parent_dir(self, parent_dir: str) -> None:
        """
        Stores the pipeline directory, to access the cdump.json other files.
        :param parent_dir: location of the parent pipeline directory.
        :return: None
        """
        self.model_parent_dir = parent_dir

    def prepare_add_node_payload_from_cdump(self, parent_dir: str, model_name: str) -> bool:
        """
        Creates the payload from cdump, when the model is added as a node to the pipeline graph on graphene.
        :param parent_dir: location of the pipeline, where the cdump is stored.
        :param model_name: the name of the model for which abovementioned details are to be found.
        :return: `True` if the operation is success, else `False`.
        """
        cdump_fp = os.path.join(parent_dir, 'cdump.json')
        cdump_dict = None
        add_node_payload = {}
        with open(cdump_fp, 'r') as cdump_file:
            cdump_dict = json.load(cdump_file)
        status = False
        for node in cdump_dict['nodes']:
            if model_name in node['nodeId']:
                del node['protoUri']
                add_node_payload = node
                status = True
                break
        with open(os.path.join(parent_dir, model_name, 'add_node_payload.json'), 'w') as add_node_fp:
            json.dump(add_node_payload, add_node_fp)

        return status

    def prepare_files_from_cdump(self, parent_dir: str, model_name: str) -> Tuple:
        """
        Retrieves the name, nodeId, and nodeSolutionId for the given model from the cdump.
        :param parent_dir: location of the pipeline, where the cdump is stored.
        :param model_name: the name of the model for which abovementioned details are to be found.
        :return: a tuple containing the node name, nodeid, and solution id of the model from the cdump.
        """
        cdump_fp = os.path.join(parent_dir, 'cdump.json')
        cdump_dict = None
        node_id = None
        node_solution_id = None
        node_name = None
        with open(cdump_fp, 'r') as cdump_file:
            cdump_dict = json.load(cdump_file)
        for node in cdump_dict['nodes']:
            if model_name in node['nodeId']:
                node_name = node['name']
                node_id = node['nodeId']
                node_solution_id = node['nodeSolutionId']
        return node_name, node_id, node_solution_id

    def download_solution(self, solution_id: str) -> Tuple:
        """
        Downloads the model.
        :param solution_id: solution id of the model to be downloaded.
        :return: the model object.
        """
        model_solution_id = solution_id
        model_solution_details = self.get_solution(solution_id=model_solution_id)
        model_name = model_solution_details['name']
        print(bordered(f'MODEL: {model_name}'))
        model_revision_id = self.get_revision_id(model_solution_id)
        model_artifacts = self.get_solution_artifacts(model_revision_id)
        docker_image_uri = None
        license_uri = None
        protobuf_uri = None
        exception = None
        model_object = None
        exception_message = None
        exception_messages = []
        try:
            if not self.check_if_solution_active(model_solution_id):
                exception_message = f'Inactive Model -- {model_name} with solution id: {model_solution_id}'
                print(exception_message)
                exception_messages.append(exception_message)
                e = NotActiveSolutionError(exception_message)
                raise e

            for artifact in model_artifacts:
                if artifact['artifactTypeCode'] == 'DI' or artifact['name'] == model_name:
                    docker_image_uri = artifact['uri']
                if artifact['name'] == "license.json" or artifact['artifactTypeCode'] == 'LI':
                    license_uri = artifact['uri']
                if artifact['name'] == "model.proto" or artifact['artifactTypeCode'] == 'MI':
                    protobuf_uri = artifact['uri']

            if docker_image_uri is None:
                exception_message = f"Invalid {model_name} Model: Missing Artifact - Docker Image URL"
                print(exception_message)
                exception_messages.append(exception_message)
                exception = NotFoundError(exception_message)
                raise exception

            if license_uri is None:
                exception_message = f"Invalid {model_name} Model: Missing Artifact - License File"
                print(exception_message)
                exception_messages.append(exception_message)
                exception = NotFoundError(exception_message)
                raise exception

            if protobuf_uri is None:
                exception_message = f"Invalid {model_name} Model: Missing Artifact - Protobuf File"
                print(exception_message)
                exception_messages.append(exception_message)
                exception = NotFoundError(exception_message)
                raise exception

            license_json_artifact = self.document_mgr.get_document(license_uri)
            protobuf_artifact = self.document_mgr.get_document(protobuf_uri)
            download_folder_location = self.model_parent_dir
            solution_download_location = os.path.join(download_folder_location, model_name)
            FileManager.create_directory(download_folder_location, model_name)
            license_file_path = os.path.join(solution_download_location, "license.json")
            protobuf_file_path = os.path.join(solution_download_location, "model.proto")
            with open(license_file_path, 'wb') as license_file:
                license_file.write(license_json_artifact['content'])
            with open(protobuf_file_path, 'wb') as protobuf_file:
                protobuf_file.write(protobuf_artifact['content'])
            model_icon = self.get_icon(model_solution_id)
            icon_file_name = self.store_icon(model_icon, solution_download_location)
            catalog_id = self.get_catalog_id(solution_id)
            description_file_path = os.path.join(solution_download_location, "description.txt")
            try:
                model_description = self.get_description(model_revision_id, catalog_id)
                with open(description_file_path, 'w') as description_file:
                    description_file.write(model_description['description'])
            except JSONDecodeError as e:
                with open(description_file_path, 'w') as description_file:
                    description_file.write("No Description Found!")
            except requests.JSONDecodeError as e:
                with open(description_file_path, 'w') as description_file:
                    description_file.write("No Description Found!")
            node_name, node_id, node_solution_id = self.prepare_files_from_cdump(self.model_parent_dir, model_name)
            self.prepare_add_node_payload_from_cdump(self.model_parent_dir, model_name)
            author_details = self.get_authors_publisher(model_solution_id, model_revision_id)
            solution_dict = {
                'authors': author_details,
                'model_name': model_name,
                'docker_uri': docker_image_uri,
                "category": model_solution_details['modelTypeCode'],
                'documents': [],
                'tags': {
                    "modelTypeCode": model_solution_details['modelTypeCode'],
                    'toolkitTypeCode': model_solution_details['toolkitTypeCode'],
                    'tags': model_solution_details['tags']
                },
                "icon_filename": icon_file_name,
                "protobuf": "model.proto",
                "license": "license.json",
                "nodeName": node_name,
                "nodeId": node_id,
                "nodeSolutionId": node_solution_id
            }

            solution_json_location = os.path.join(solution_download_location, 'solution.json')
            with open(solution_json_location, 'w') as sol_json_file:
                json.dump(solution_dict, sol_json_file)
            model_object = Model(model_name, solution_dict, solution_download_location, True)
        except NotFoundError as e:
            return model_name, model_object, exception_messages
        except NotActiveSolutionError as e:
            return model_name, model_object, exception_messages
        return model_name, model_object, None
