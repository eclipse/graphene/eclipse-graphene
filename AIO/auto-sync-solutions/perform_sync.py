import argparse
import os
import shutil
import zipfile
from glob import glob

from flask import Flask, render_template, request

from auto_export_solutions import solution_downloader
from auto_import_solutions import solution_uploader
from auto_sync_common.utils import EnvVarsManager
from perform_sync.sync import PerformSync

app = Flask(__name__, template_folder="perform_sync/templates", static_folder="perform_sync/static")
app.config["TEMPLATES_AUTO_RELOAD"] = True

parser = argparse.ArgumentParser(description='Auto-Sync CLI Tool')
parser.add_argument('-e', '--env-vars',
                    help='Root Directory where eclipse-graphene is installed. ' +
                         'This will help in locating the environment variables and uploading tutorials.' +
                         'Default value would be "/home/ai4eu/eclipse-graphene"',
                    required=False, default="/home/ai4eu/eclipse-graphene/AIO/")
args = parser.parse_args()
env_vars_mgr = EnvVarsManager(args.env_vars)
target_env_vars = env_vars_mgr.target_env_config
sync_performer = PerformSync(target_env_vars)


@app.route('/')
def index():
    return render_template('index.html')


@app.post('/download_solutions')
def download_solutions():
    solution_ids = request.form.getlist('pipelines')
    os.environ['SYNC_MODE'] = 'DOWNLOAD'
    target_env_vars['SOLUTION_IDS'] = solution_ids
    status = {}
    downloaded_pipelines = solution_downloader.download_pipelines(target_env_vars)
    for pipeline_name, pipeline_object, pipeline_exception in downloaded_pipelines:
        if pipeline_object is not None:
            status[pipeline_name] = {"status": 'succeeded'}
        else:
            status[pipeline_name] = {'status': 'failed', 'reason(s)': pipeline_exception}
            print('Some downloaded pipelines are invalid - they contained invalid or missing artifacts!')
    if downloaded_pipelines is None or len(downloaded_pipelines) == 0:
        print(
            'All pipelines to be downloaded is invalid!'
            ' Please provide another source pipeline solution id and rerun this program!')
    del os.environ['SYNC_MODE']
    return status


@app.route('/tutorials')
def landing_tutorial():
    tutorials_list = sync_performer.get_list_of_tutorial_pipelines()
    return render_template('auto-import-tutorials.html', tutorials=tutorials_list)


@app.route('/import_tutorials_landing', methods=['POST'])
def import_tutorials():
    os.environ['SYNC_MODE'] = 'TUTORIALS'
    json_file_path = "./auto_import_solutions/tutorial_solutions.json"
    pipelines_status = solution_uploader.upload_solutions(target_env_vars, json_file_path)
    print('Importing tutorials!')
    del os.environ['SYNC_MODE']
    return pipelines_status


@app.route('/export_solutions')
def export_solutions():
    active_pipelines_list = sync_performer.get_all_solutions_from_all_catalogs()
    return render_template('auto-export.html', pipelines=active_pipelines_list)


@app.route('/import_solutions')
def import_solutions():
    return render_template('auto-import.html')


@app.route('/upload_solutions', methods=['POST'])
def upload_solutions():
    os.environ['SYNC_MODE'] = 'UPLOAD'
    json_file_path = './auto_import_solutions/downloaded_solutions/downloaded_solutions.json'
    with zipfile.ZipFile('auto_export_solutions/downloaded_solutions.zip', 'r') as zip_ref:
        zip_ref.extractall('auto_import_solutions/downloaded_solutions/')
    for element in glob('auto_import_solutions/downloaded_solutions/*'):
        if os.path.isdir(element):
            if 'invalid_pipeline.json' in os.listdir(element):
                print(f"deleting {element}, invalid pipeline")
                shutil.rmtree(element)
    pipelines_status = solution_uploader.upload_solutions(target_env_vars, json_file_path)
    del os.environ['SYNC_MODE']
    return pipelines_status


if __name__ == "__main__":
    app.run(debug=False)
