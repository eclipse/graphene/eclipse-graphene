#source ../graphene_env.sh

# check if the this script is called in the context of renew-letsencrypt or installation
if [ -z "$GRAPHENE_USER" ]; then
  # we are in installation context
  sudo cp /etc/letsencrypt/live/$GRAPHENE_DOMAIN/privkey.pem graphene.key
  sudo chown $USER:$USER graphene.key
  sudo cp /etc/letsencrypt/live/$GRAPHENE_DOMAIN/fullchain.pem graphene.crt
  sudo chown $USER:$USER graphene.crt
fi
rm -f *store*

