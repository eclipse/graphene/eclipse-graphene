### The installation consists of 3 supportive services and 10 Graphene services.

#### The 10 Graphene services are:
* cds  (Common Data Service)
* dsce (Design Studio Compostion Engine)
* federation
* kubernetes-client
* license-profile-editor
* license-rtu-editor
* onboarding
* playground-deployer
* portal-be (Portal Backend)
* portal-fe (Portal Frontend)

All of these microservices are installed as deployment + service

And some have additionally an ingress, see folder **eclipse-graphene/AIO/ingress** 
* cds-ingress
* federation-ingress
* kubernetes-client-ingress
* license-profile-editor-ingress
* onboarding-ingressd
* playground-deployer-ingress
* portal-ingress    

The services interact with each other, so the need to know service-endpoints and credentials. That leads to a lot of configuration parameters that are passed in the deployment.yaml files. 

#### And the 3 supportive services are:
* graphene-ingress-nginx-controller (Not needed in Rancher installations)
* graphene-mariadb (SQL Database)
* nexus (File Storage)

ingress-controller and mariadb are installed via helm, see folder **eclipse-graphene/charts**

mariadb needs additional setup to setup users and passowrds. After that, the database schema needs to be initialized with this file from the comen data service: **common-dataservice/cmn-data-svc/cmn-data-svc-server/db-scripts/cmn-data-svc-ddl-dml-3.3.sql**

Nexus stores the model files like the proto-files, license.json or blueprint.json and execution-run.json for pipelines.

Nexus setup has the challenge to fetch the initial password from the nexus pod, a unique one is automatically created there. Also the repository needs to be configured to allow multiple uploads for a files.

Both storage services (nexus and mariadb) need persistent volumes to store the data, which are created by the installation scripts.

#### Configuration values
The configuration values are stored in three files:
* eclipse-graphene/AIO/graphene_env.sh
* eclipse-graphene/AIO/mariadb_env.sh
* eclipse-graphene/AIO/nexus_env.sh
These could be seen as the source to create helm values files

Each of the 10 graphene microservices have templates for the deployment.yaml and the service.yaml in the folder **eclipse-graphene/AIO/kubernetes** 

The installtion applies the configuration values to the templates and stores the results in the folder **eclipse-graphene/AIO/deploy**