#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2022 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================
import requests
from dotenv import dotenv_values
import mysql.connector



class BlueprintMigrator(object):
    """
    This class reads the list of existing composite solutions and re-creates
    the blueprint.json by calling the design-studio backend
    """

    def __init__(self):
        # setup parameters from reading environment variables.
        self.config = {
            **dotenv_values('../graphene_env.sh'), **dotenv_values('../mariadb_env.sh')
        }

        self.host = f'https://{self.config["GRAPHENE_DOMAIN"]}'
        self.cnx = mysql.connector.connect(user='root',
                                      password=self.config['GRAPHENE_MARIADB_PASSWORD'],
                                      host=self.config['GRAPHENE_MARIADB_ADMIN_HOST'],
                                      port=self.config['GRAPHENE_MARIADB_NODEPORT'],
                                      database='graphene_cds')

    def migrate(self, row):
        version=row[1]
        user_id=row[6]
        solution_id=row[3][10:]
        desc=row[4][21:]
        solution_name=desc[0:desc.find(' for')]
        read_solution_api=self.host+f'/dsce/dsce/solution/readCompositeSolutionGraph?userId={user_id}&solutionId={solution_id}&version={version}'
        response = requests.get(url=read_solution_api)
        print(f'read: {len(response.content)}')

        validate_solution_api = self.host+f'/dsce/dsce/solution/validateCompositeSolution'
        params = {
            "userId": user_id,
            "solutionName": solution_name,
            "solutionId": solution_id,
            "version": version
        }
        response = requests.post(url=validate_solution_api, params=params)
        print(f'validate {solution_name}: {response.text}')

    def process_blueprint_list(self):
        bplist = self.cnx.cursor(buffered=True)
        bplist.execute("select distinct B.* from C_ARTIFACT B, C_ARTIFACT C where B.ARTIFACT_TYPE_CD = 'BP' and C.ARTIFACT_TYPE_CD = 'CD' and substring_index(B.NAME,'-',-5) = substring_index(C.NAME,'-',-5) and B.VERSION=C.VERSION")
        print(f'solution count: {bplist.rowcount}')
        for row in bplist:
            self.migrate(bplist.next())


if __name__ == '__main__':
    try:
        migrator=BlueprintMigrator()
        migrator.process_blueprint_list()
    except Exception as x:
        print(x)
