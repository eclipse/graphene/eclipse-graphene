#!/bin/bash
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2018 AT&T Intellectual Property. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================
#
# What this is: script that enables use of pre-configured CA and server
# certificates for an Graphene platform, or creation of new self-signed
# certificates.
#
# Prerequisites:
# - graphene_env.sh script prepared through oneclick_deploy.sh or manually, to
#   set install options (e.g. docker/k8s)
# - host folder /mnt/$GRAPHENE_NAMESPACE/certs created through setup_pv.sh
#
# Usage:
#   For docker-based deployments, run this script on the AIO host.
#   For k8s-based deployment, run this script on the AIO host or a workstation
#   connected to the k8s cluster via kubectl (e.g. via tools/setup_kubectl.sh)
#   $ bash setup_keystore.sh
#
function update_cert_env() {
  trap 'fail' ERR
  log "Updating graphene_env.sh with \"export $1=$2\""
  sedi "s/$1=.*/$1=$2/" $AIO_ROOT/graphene_env.sh
  export $1=$2
}

function setup_keystore() {
  trap 'fail' ERR
  if [[ -e /etc/letsencrypt/live ]]; then
    cd certs
    bash setup_letsencrypt.sh
    cd ..
  else
    if [[ -e certs/$GRAPHENE_CERT ]]; then
      log "Using existing user-prepared files in certs subfolder"
    else
      log "Creating new certs in certs subfolder"
      cd certs
      if [[ "$GRAPHENE_DOMAIN_IP" == "$GRAPHENE_HOST_IP" ]]; then
        extra_ips="$GRAPHENE_DOMAIN_IP"
      else
        extra_ips="$GRAPHENE_DOMAIN_IP $GRAPHENE_HOST_IP"
      fi
      bash setup_certs.sh $GRAPHENE_CERT_PREFIX $GRAPHENE_CERT_SUBJECT_NAME \
        "$GRAPHENE_HOST" "$extra_ips"
      cd ..
    fi
  fi

  if [[ "$DEPLOYED_UNDER" == "docker" ]]; then
    if [[ ! -e /mnt/$GRAPHENE_NAMESPACE/certs ]]; then
      log "Folder /mnt/$GRAPHENE_NAMESPACE/certs was not found"
      fail "Please have a host Admin run setup_prereqs.sh before this script"
    fi
    cp $(ls certs/* | grep -v '\.sh') /mnt/$GRAPHENE_NAMESPACE/certs/.
  else
    log "Create kubernetes configmap to hold the keystore and truststore"
    # See use in deployment templates for portal-be and federation
    if [[ $(kubectl get configmap -n $GRAPHENE_NAMESPACE graphene-certs) ]]; then
      log "Delete existing graphene-certs configmap in case cert changes were made"
      kubectl delete configmap -n $GRAPHENE_NAMESPACE graphene-certs
    fi
    kubectl create configmap -n $GRAPHENE_NAMESPACE graphene-certs \
      --from-file=certs/$GRAPHENE_CERT
  fi
}

set -x
trap 'fail' ERR
WORK_DIR=$(pwd)
cd $(dirname "$0")
source utils.sh
source graphene_env.sh
setup_keystore
cd $WORK_DIR
