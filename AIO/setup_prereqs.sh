#!/bin/bash
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2017-2019 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T and Tech Mahindra
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================
#
# What this is: Prerequisite setup script for All-in-One (AIO) deployment of the
# Graphene platform. Intended to support users who do not have sudo permission, to
# have a host admin (sudo user) run this script in advance for them.
# FOR TEST PURPOSE ONLY.
#
# Prerequisites:
# - Ubuntu Xenial (16.04), Bionic (18.04), or Centos 7 hosts
# - All hostnames specified in graphene_env.sh must be DNS-resolvable on all hosts
#   (entries in /etc/hosts or in an actual DNS server)
# - User running this script has:
#   - Installed docker per eclipse-graphene/tools/setup_docker.sh
#   - Added themselves to the docker group (sudo usermod -aG docker $USER)
#   - Logged out and back in, to activate docker group membership
# - If deploying in preparation for use by a non-sudo user
#   - Created the user account (sudo useradd -m <user>)
# - eclipse-graphene repo clone (patched, as needed) in home folder
#
# Usage:
# $ cd eclipse-graphene/AIO
# $ bash setup_prereqs.sh <under> <domain> <user> [k8s_dist]
#   under: docker|k8s; install prereqs for docker or k8s based deployment
#   domain: FQDN of platform
#   user: user that will be completing Graphene platform setup via
#         oneclick_deploy.sh (if installing for yourself, use $USER)
#   k8s_dist: k8s distribution (generic|openshift), required for k8s deployment
#

function wait_dpkg() {
  # TODO: workaround for "E: Could not get lock /var/lib/dpkg/lock - open (11: Resource temporarily unavailable)"
  echo; echo "waiting for dpkg to be unlocked"
  while sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; do
    sleep 1
  done
}

function setup_prereqs() {
  trap 'fail' ERR

  log "/etc/hosts customizations"
  # Ensure cluster hostname resolves inside the cluster
  check_name_resolves $GRAPHENE_DOMAIN
  if [[ "$NAME_RESOLVES" == "false" ]]; then
    if [[ $(grep -c -E " $GRAPHENE_DOMAIN( |$)" /etc/hosts) -eq 0 ]]; then
      log "Add $GRAPHENE_DOMAIN to /etc/hosts"
      echo "$GRAPHENE_HOST_IP $GRAPHENE_DOMAIN" | sudo tee -a /etc/hosts
    fi
  fi

  log "/etc/hosts:"
  cat /etc/hosts

  log "Basic prerequisites"
  if [[ "$HOST_OS" == "ubuntu" ]]; then
    # This us needed to avoid random errors ala "no release file" when trying to
    # update apt, after prior mariadb install using one of the mariadb mirrors.
    # The mirrors may become unreliable, thus the MARIADB_MIRROR env param
    log "Remove any prior reference to mariadb in /etc/apt/sources.list"
    sudo sed -i -- '/mariadb/d' /etc/apt/sources.list
    wait_dpkg; sudo apt-get update
    # TODO: fix need to skip upgrade as this sometimes updates the kube-system
    # services and they then stay in "pending", blocking k8s-based deployment
    # Also on bionic can cause a hang at 'Preparing to unpack .../00-systemd-sysv_237-3ubuntu10.11_amd64.deb ...'
    #  wait_dpkg; sudo apt-get upgrade -y
    wait_dpkg; sudo apt-get install -y wget git jq netcat-traditional python3-dotenv python3-mysql.connector python3-flask python3-numpy python3-pil
  else
    # For centos, only deployment under k8s is supported
    # docker is assumed to be pre-installed as part of the k8s install process
    if [[ "$K8S_DIST" == "openshift" ]]; then
      log "Skipping yum update since that may update docker and break the OpenShift cluster..."
    else
      sudo yum -y update
    fi
    sudo rpm -Fvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
    sudo yum install -y wget git jq bind-utils nmap-ncat
  fi

  log "Setup Graphene data home at /mnt/$GRAPHENE_NAMESPACE"
  sudo mkdir -p /mnt/$GRAPHENE_NAMESPACE
  sudo chown $GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER /mnt/$GRAPHENE_NAMESPACE
}

function prepare_mariadb() {
  trap 'fail' ERR

  # Do not reset mariadb service/data unless deploying via oneclick_deploy
  if [[ "$GRAPHENE_DEPLOY_MARIADB" == "true" ]]; then
    if [[ ! -e mariadb_env.sh ]]; then
      cd $AIO_ROOT/../charts/mariadb/
      source setup_mariadb_env.sh
      cp mariadb_env.sh $AIO_ROOT/.
      cd $AIO_ROOT
    fi

    log "Stop any existing components for mariadb-service"
    if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
      bash $WORK_DIR/eclipse-graphene/charts/mariadb/setup_mariadb.sh \
        clean $(hostname) $K8S_DIST
      bash $WORK_DIR/eclipse-graphene/charts/mariadb/setup_mariadb.sh \
        prep $(hostname) $K8S_DIST
    else
      bash mariadb/docker_compose.sh down
      setup_docker_volume /mnt/$GRAPHENE_MARIADB_NAMESPACE/$GRAPHENE_MARIADB_DATA_PV_NAME \
        "$GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER"
    fi
  elif [[ ! -e mariadb_env.sh ]]; then
    fail "No mariadb_env.sh found. Please provide one or set GRAPHENE_DEPLOY_MARIADB=true"
  fi
}

function prepare_nexus() {
  trap 'fail' ERR
  if [[ "$GRAPHENE_DEPLOY_NEXUS" == "true" ]]; then
    if [[ ! -e nexus_env.sh ]]; then
      cd $AIO_ROOT/nexus
      source setup_nexus_env.sh
      cp nexus_env.sh $AIO_ROOT/.
      cd $AIO_ROOT
    fi
    if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
      bash $WORK_DIR/eclipse-graphene/AIO/nexus/setup_nexus.sh clean
      bash $WORK_DIR/eclipse-graphene/AIO/nexus/setup_nexus.sh prep
      # Apply updates from above
      source $AIO_ROOT/graphene_env.sh
    else
      setup_docker_volume /mnt/$GRAPHENE_NEXUS_NAMESPACE/$GRAPHENE_NEXUS_DATA_PV_NAME \
        "200:$GRAPHENE_HOST_USER"
    fi
  fi
}

function prepare_ingress() {
  trap 'fail' ERR
  if [[ "$DEPLOYED_UNDER" == "docker" ]]; then
    setup_docker_volume /mnt/$GRAPHENE_NAMESPACE/$KONG_DB_PV_NAME \
      "$GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER"
  fi
}

function prepare_graphene() {
  trap 'fail' ERR
  if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
    create_namespace $GRAPHENE_NAMESPACE
    if [[ "$K8S_DIST" == "openshift" ]]; then
      fix_openshift_uidgid_range
    fi
  fi

  if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
    if [[ "$GRAPHENE_CREATE_PVS" == "true" ]]; then
      if [[ "$GRAPHENE_CREATE_PVS" == "true" && "$GRAPHENE_PVC_TO_PV_BINDING" == "true" ]]; then
        bash $AIO_ROOT/../tools/setup_pv.sh all /mnt/$GRAPHENE_NAMESPACE \
          $GRAPHENE_LOGS_PV_NAME $GRAPHENE_LOGS_PV_SIZE \
          "$GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER"
      fi
    fi
  else
    setup_docker_volume /mnt/$GRAPHENE_NAMESPACE/$GRAPHENE_LOGS_PV_NAME \
      "$GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER"
    log "Prepare the sv-scanning configmap folder"
    if [[ ! -e /mnt/$GRAPHENE_NAMESPACE/sv ]]; then
      sudo mkdir /mnt/$GRAPHENE_NAMESPACE/sv
      sudo chown $GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER /mnt/$GRAPHENE_NAMESPACE/sv
    fi
  fi
}

function prepare_env() {
  trap 'fail' ERR
  sed -i -- "s/DEPLOY_RESULT=.*/DEPLOY_RESULT=/" graphene_env.sh
  sed -i -- "s/FAIL_REASON=.*/FAIL_REASON=/" graphene_env.sh
  update_graphene_env DEPLOYED_UNDER $1 force
  update_graphene_env GRAPHENE_DOMAIN $2 force
  update_graphene_env GRAPHENE_HOST_USER $3 force
  if [[ "$DEPLOYED_UNDER" == "docker" ]]; then
    setup_docker
  else
    update_graphene_env K8S_DIST "$4" force
    set_k8s_env
    if [[ "$K8S_DIST" == "openshift" ]]; then
      oc login -u $GRAPHENE_OPENSHIFT_USER -p $GRAPHENE_OPENSHIFT_PASSWORD
      log "Workaround: Graphene AIO requires privilege to set PV permissions or run as root where needed"
      oc adm policy add-scc-to-user privileged -z default -n $GRAPHENE_NAMESPACE
      # PV recyclers run in the default namespace and need privileged also
      oc adm policy add-scc-to-user privileged -z default -n default
    else
      # OpenShift creates a set of PVs - generic k8s does not
      setup_utility_pvs 10 "1Gi 5Gi 10Gi"
    fi
  fi

  update_graphene_env GRAPHENE_HOST $(hostname) force
  GRAPHENE_HOST_IP=$(/sbin/ip route get 8.8.8.8 | head -1 | sed 's/^.*src //' | awk '{print $1}')
  update_graphene_env GRAPHENE_HOST_IP $GRAPHENE_HOST_IP force
  get_host_ip $GRAPHENE_DOMAIN
  update_graphene_env GRAPHENE_DOMAIN_IP $HOST_IP force
  get_host_info
  update_graphene_env GRAPHENE_HOST_OS $HOST_OS
  update_graphene_env GRAPHENE_HOST_OS_VER $HOST_OS_VER
  source $AIO_ROOT/graphene_env.sh
}

if [[ $# -lt 3 ]]; then
  cat <<'EOF'
Usage:
  $ cd eclipse-graphene/AIO
  $ bash setup_prereqs.sh <under> <domain> <user> [k8s_dist]
    under: docker|k8s; install prereqs for docker or k8s based deployment
    domain: FQDN of platform
    user: user that will be completing Graphene platform setup via
          oneclick_deploy.sh (if installing for yourself, use $USER)
    k8s_dist: k8s distribution (generic|openshift), required for k8s deployment
EOF
  echo "All parameters not provided"
  exit 1
fi

set -x
trap 'fail' ERR
WORK_DIR=$(pwd)
cd $(dirname "$0")
source utils.sh
update_graphene_env AIO_ROOT $(pwd) force
update_graphene_env DEPLOY_RESULT "" force
update_graphene_env FAIL_REASON "" force
cat <<EOF >status.sh
EOF
source graphene_env.sh
verify_ubuntu_or_centos

prepare_env $1 $2 $3 $4
setup_prereqs
prepare_mariadb
bash $AIO_ROOT/../tools/setup_mariadb_client.sh
prepare_ingress
prepare_graphene
# Nexus has to precede docker-engine prep as it sets GRAPHENE_DOCKER_REGISTRY_HOST
prepare_nexus

mkdir -p $AIO_ROOT/../../graphene/env $AIO_ROOT/../../graphene/certs $AIO_ROOT/../../graphene/logs
cp $AIO_ROOT/*_env.sh $AIO_ROOT/../../graphene/env/.

if [[ "$GRAPHENE_HOST_USER" != "$USER" ]]; then
  log "Add $GRAPHENE_HOST_USER to the docker group"
  sudo usermod -a -G docker $GRAPHENE_HOST_USER
  # Setup the graphene user env
  if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
    sudo cp -r ~/.kube /home/$GRAPHENE_HOST_USER/.
    sudo chown -R $GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER /home/$GRAPHENE_HOST_USER/.kube
  fi
  mkdir -p $AIO_ROOT/../../graphene/env/clean/
  cp $AIO_ROOT/../../graphene/env/*.sh $AIO_ROOT/../../graphene/env/clean/.
  sudo cp -r $AIO_ROOT/../../graphene /home/$GRAPHENE_HOST_USER/.
  sudo chown -R $GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER /home/$GRAPHENE_HOST_USER/graphene
  sudo cp -r $AIO_ROOT/../../eclipse-graphene /home/$GRAPHENE_HOST_USER/.
  sudo chown -R $GRAPHENE_HOST_USER:$GRAPHENE_HOST_USER /home/$GRAPHENE_HOST_USER/eclipse-graphene
fi

set +x
cd $AIO_ROOT
cat <<EOF >status.sh
DEPLOY_RESULT=prepped
FAIL_REASON=
EOF
update_graphene_env DEPLOY_RESULT prepped force

log "Prerequisites setup is complete."
if [[ -e /tmp/json ]]; then sudo rm /tmp/json; fi
if [[ -e /tmp/graphene ]]; then sudo rm -rf /tmp/graphene; fi
cd $AIO_ROOT
