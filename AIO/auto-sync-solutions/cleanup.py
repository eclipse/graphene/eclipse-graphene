from glob import glob
import shutil
import os


def cleanup():
    download_locations = glob("auto_export_solutions/downloaded_solutions/*/")
    download_locations+= glob("auto_export_solutions/downloaded_solutions/*.*")
    download_locations.append("auto_export_solutions/downloaded_solutions.zip")
    download_locations+= glob("auto_import_solutions/downloaded_solutions/*/")
    download_locations += glob("auto_import_solutions/downloaded_solutions/*.*")
    print(download_locations)
    for path in download_locations:
        if os.path.exists(path):
            if os.path.isdir(path):
                shutil.rmtree(path)
            else:
                os.remove(path)


if __name__ == "__main__":
    cleanup()
