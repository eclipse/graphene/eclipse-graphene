#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2024 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import json
import os
from pathlib import Path
from typing import Dict, Tuple
from typing import Union
from urllib.parse import urljoin

import requests
from dotenv import dotenv_values

default_header = {
    "Content-Type": "application/json"
}


class APIClient:
    """
    This class is used to make API calls. All the method in this class log all addresses by default.
    """

    def __init__(
            self,
            username: str,
            password: str,
            base_url: str,
    ) -> None:
        self.header_type = {
            "Content-Type": "application/json"
        }
        self.user = username
        self.psw = password
        self.base_url = base_url
        self.auth_data = (self.user, self.psw)

    def build_url(self, url_target: str) -> str:
        """
        Builds url by appending the url target to the base host and port.
        :param url_target: target url to be reached
        :returns: url with format http://base_url:port/url_target
        """
        return urljoin(self.base_url, url_target)

    def post_request(self,
                     api: str,
                     data: Dict = None,
                     params: Dict = None,
                     files: Dict = None,
                     headers: Dict = default_header,
                     data_json: Dict = None) -> requests.Response:
        """
        Creates POST request with provided api, data and setting necessary headers.
        :param api: target url to be called for creating POST request.
        :param data: dictionary format of data (payload) to create POST request.
        :param params: dictionary format of data for query params to create POST request.
        :param files: the files to be stored to be provided as dictionary.
        :param headers: additional headers to be set for the POST request.
        :param data_json: json payload to the post request as the dictionary object.
        :returns: dictionary representing the json response of the API call.
        """
        api = self.build_url(api)
        post_response = requests.post(api,
                                      auth=self.auth_data,
                                      headers=headers,
                                      data=data,
                                      params=params,
                                      files=files,
                                      json=data_json
                                      )
        print(f"POST REQUEST WITH RESPONSE CODE {post_response.status_code} AT: {api}")
        return post_response

    def post_document_request(self, api: str, params: Tuple, files: Dict, headers: Dict = None) -> object:
        """
        Creates POST request to the provided api, storing the files with provided params.
        :param api: target url to be called for creating POST request.
        :param params: Tuple of params, in key value pair.
        :param files: the files to be stored to be provided as dictionary.
        :param headers: additional headers to be set for the POST request.
        :returns: response object of the API call.
        """
        api = self.build_url(api)
        response = requests.post(api,
                                 params=params,
                                 files=files,
                                 auth=self.auth_data,
                                 headers=headers)
        print(f"POST DOCUMENT REQUEST WITH RESPONSE CODE {response.status_code} AT: {api}")
        return response

    def post_document_request_wo_auth(self, api: str, params: Tuple, files: Dict, headers: Dict = None) -> object:
        """
        Creates POST request to the provided api, storing the files with provided params, excluding the authentication tokens.
        Useful for onboarding solutions, where auth tokens are not needed.
        :param api: target url to be called for creating POST request.
        :param params: Tuple of params, in key value pair.
        :param files: the files to be stored to be provided as dictionary.
        :param headers: additional headers to be set for the POST request.
        :returns: response object of the API call.
        """
        api = self.build_url(api)
        response = requests.post(api,
                                 params=params,
                                 files=files,
                                 headers=headers)
        print(f"POST DOCUMENT REQUEST WO AUTH WITH RESPONSE CODE {response.status_code} AT: {api}")
        return response

    def put_request(self, api: str, data: Dict, headers: Dict = default_header):
        """
        Creates PUT request to the provided api, storing the data with provided params.
        :param api: target url to be called for creating PUT request.
        :param data: dictionary format of data to create PUT request.
        :param headers: additional headers to be set for the PUT request.
        :returns: dictionary representing the json response of the API call.
        """
        api = self.build_url(api)
        put_response = requests.put(api,
                                    auth=self.auth_data,
                                    headers=headers,
                                    data=json.dumps(data)
                                    )
        print(f"PUT REQUEST WITH RESPONSE CODE {put_response.status_code} AT: {api}")
        return put_response.json()

    def put_image_request(self, api: str, data: str, headers: Dict = default_header):
        """
        Creates PUT request to the provided api, storing the image with provided params.
        :param api: target url to be called for creating PUT request.
        :param data: image as raw bytes string.
        :param headers: additional headers to be set for the PUT request.
        :returns: dictionary representing the json response of the API call.
        """
        api = self.build_url(api)
        put_response = requests.put(api,
                                    auth=self.auth_data,
                                    headers=headers,
                                    data=data
                                    )
        print(f"PUT IMAGE REQUEST WITH RESPONSE CODE {put_response.status_code} AT: {api}")
        return put_response.json()

    def get_put_request(self, api: str, data: Dict, headers: Dict = default_header):
        """
        First makes a get request to retrieve the existing data.
        Then updates the received information with data.
        Lastly, creates PUT request to the provided api, updating the data with provided params.
        :param api: target url to be called for creating PUT request.
        :param data: dictionary format of data to create PUT request.
        :param headers: additional headers to be set for the PUT request.
        :returns: dictionary representing the json response of the API call.
        """
        get_response = self.get_request(api)
        api = self.build_url(api)
        put_response = requests.put(api,
                                    auth=self.auth_data,
                                    headers=headers,
                                    data=json.dumps({**get_response, **data})
                                    )
        print(f"PUT REQUEST WITH RESPONSE CODE {put_response.status_code} AT: {api}")
        return put_response.json()

    def get_request(
            self,
            api: str,  # url at which request is to be sent
    ) -> Dict:
        """
        Creates GET request to retrieve data from the provided API.
        :param api: target url to be called for retrieving data from GET request.
        :returns: dictionary representing the json response of the API call.
        """
        api_url = self.build_url(api)
        response = requests.get(api_url, auth=self.auth_data)
        print(f"GET REQUEST WITH RESPONSE CODE {response.status_code} AT: {api_url}")
        return response.json()

    def get_document(
            self,
            api: str,  # url at which request is to be sent
    ) -> Dict:
        """
        Creates GET request to retrieve document from the provided API.
        :param api: target url to be called for retrieving data from GET request.
        :returns: dictionary representing the json response of the API call.
        """
        api_url = self.build_url(api)
        response = requests.get(api_url, auth=self.auth_data)
        file_name = response.url.split('/')[-1]
        version = response.url.split('/')[-2]
        file_name_wo_version = file_name.replace(f'-{version}', '')
        document = {
            'content': response.content,
            'file_name': file_name,
            'file_name_wo_version': file_name_wo_version,
            'version': version
        }
        print(f"GET DOCUMENT REQUEST WITH RESPONSE CODE {response.status_code} AT: {api_url}")
        return document

    def get_image(
            self,
            api: str,  # url at which request is to be sent
    ) -> object:
        """
        Retrieves image from the host.
        :param api: target url to be called for retrieving data from GET request.
        :returns: icon image as a bytes string object from response of the API call.
        """
        api_url = self.build_url(api)
        response = requests.get(api_url, auth=self.auth_data)
        print(f"GET IMAGE REQUEST WITH RESPONSE CODE {response.status_code} AT: {api_url}")
        return response.content


class FileManager(object):
    """
    This class manages all files and folder related operations.
    """

    @staticmethod
    def create_directory(location: str, folder_name: str) -> bool:
        """
        This method will create the folder at the specified location.
        :param location: this is the location to create the new directory.
        :param folder_name:
        :return: `True` if the directory was created else `False`
        """
        new_folder_path = os.path.join(location, folder_name)
        if not os.path.exists(new_folder_path):
            os.makedirs(new_folder_path)
            return True

    @staticmethod
    def del_dir(target: Union[Path, str], only_if_empty: bool = False):
        """
        Delete a given directory and its subdirectories.

        :param target: The directory to delete
        :param only_if_empty: Raise RuntimeError if any file is found in the tree
        """
        target = Path(target).expanduser()
        assert target.is_dir()
        for p in sorted(target.glob('**/*'), reverse=True):
            if not p.exists():
                continue
            p.chmod(0o666)
            if p.is_dir():
                p.rmdir()
            else:
                if only_if_empty:
                    raise RuntimeError(f'{p.parent} is not empty!')
                p.unlink()
        target.rmdir()


def find_user_id(api_client, graphene_token):
    """
    This function retrieves the user id by using the provided graphene token.
    :param api_client: the api client to create a REST API call to the graphene host.
    :param graphene_token: graphene token of the user.
    :return user_id: a string that is user id retrieved from graphene host.
    """
    api_link = f"ccds/user/search?loginName={graphene_token.split(':')[0]}"
    resp = api_client.get_request(api_link)
    user_id = resp['content'][0]['userId']
    return user_id


def find_catalog_id(api_client):
    """
    This function retrieves the default public catalog id present at the graphene host.
    :param api_client: the api client to create a REST API call to the graphene host.
    :return catalog_id: a string that is catalog id of default public catalog available at the graphene host.
    """
    api_link = f"ccds/catalog"
    resp = api_client.get_request(api_link)
    catalogs = resp['content']
    catalog_id = ""
    for catalog in catalogs:
        if catalog['accessTypeCode'] == "PB":
            catalog_id = catalog['catalogId']
            break
    return catalog_id


class EnvVarsManager(object):
    """
    This class is responsible to manage environment variables required for auto-sync cli to work.
    It has two main environment configurations:
    1. `target_env_config`: the graphene platform, where the onboarding of either pipelines or tutorials takes place.
    2. `source_env_config`: the graphene platform, from which we will export required pipelines.
    It sets all the required envrionment variables, when the auto-sync is initialized.
    """

    def __init__(self, install_dir: str = "/home/ai4eu/eclipse-graphene/AIO"):
        self.install_dir = install_dir
        graphene_envs = dotenv_values(os.path.join(install_dir, "graphene_env.sh"))
        nexus_envs = dotenv_values(os.path.join(install_dir, "nexus_env.sh"))
        import_envs = dotenv_values(os.path.join(install_dir, "import_env.sh"))
        self.source_host = None
        self.source_api_client = None
        self.src_env_config = {}
        self.target_env_config = {
            # Graphene Env Vars
            'GRAPHENE_DOC_USERID': graphene_envs['GRAPHENE_OPERATOR_ID'],
            'GRAPHENE_HOST': graphene_envs['GRAPHENE_DOMAIN'],
            'GRAPHENE_PW': graphene_envs['GRAPHENE_CDS_PASSWORD'],
            'GRAPHENE_USER': graphene_envs['GRAPHENE_CDS_USER'],

            # Nexus Env Vars
            'NEXUS_HOST': nexus_envs['GRAPHENE_NEXUS_HOST'],
            'NEXUS_PORT': nexus_envs['GRAPHENE_NEXUS_API_PORT'],
            'NEXUS_REPO_NAME': nexus_envs['GRAPHENE_NEXUS_MAVEN_REPO'],
            'NEXUS_ROOT_GROUP_ID': nexus_envs['GRAPHENE_NEXUS_GROUP'],
            'NEXUS_RW_PASSWORD': nexus_envs['GRAPHENE_NEXUS_RW_USER_PASSWORD'],
            'NEXUS_RW_USERNAME': nexus_envs['GRAPHENE_NEXUS_RW_USER'],

            # Default Env Vars
            'SYNC_MODE': 'TUTORIALS',
            'REGISTRY_HOST': import_envs['REGISTRY_HOST'] if 'REGISTRY_HOST' in import_envs else 'cicd.ai4eu-dev.eu',

            # Env Vars to be set by User
            'GRAPHENE_TOKEN': import_envs['GRAPHENE_TOKEN'],
        }

        if 'SRC_GRAPHENE_HOST' in import_envs:
            self.load_source_env_vars(import_envs)

        self.target_host = f'https://{self.target_env_config["GRAPHENE_HOST"]}'
        self.target_api_client = APIClient(self.target_env_config['GRAPHENE_USER'],
                                           self.target_env_config['GRAPHENE_PW'], self.target_host)

        if 'TEST_MODE' in import_envs:
            self.target_env_config['TEST_MODE'] = True
            os.environ['TEST_MODE'] = 'true'
        else:
            self.target_env_config['TEST_MODE'] = False

        if 'SYNC_MODE' in import_envs and import_envs['SYNC_MODE']:
            self.target_env_config['SYNC_MODE'] = import_envs['SYNC_MODE']
            os.environ['SYNC_MODE'] = import_envs['SYNC_MODE']

        user_id = find_user_id(self.target_api_client, self.target_env_config['GRAPHENE_TOKEN'])
        catalog_id = find_catalog_id(self.target_api_client)
        self.target_env_config['GRAPHENE_USERID'] = user_id
        os.environ['GRAPHENE_USERID'] = user_id
        self.target_env_config['GRAPHENE_CATALOGID'] = catalog_id

    def load_source_env_vars(self, import_envs):
        self.src_env_config = {
            # Graphene Env Vars
            'GRAPHENE_DOC_USERID': import_envs['SRC_GRAPHENE_DOC_USERID'],
            'GRAPHENE_HOST': import_envs['SRC_GRAPHENE_HOST'],
            'GRAPHENE_PW': import_envs['SRC_GRAPHENE_PW'],
            'GRAPHENE_USER': import_envs['SRC_GRAPHENE_USER'],

            # Nexus Env Vars
            'NEXUS_HOST': import_envs['SRC_NEXUS_HOST'],
            'NEXUS_PORT': import_envs['SRC_NEXUS_PORT'],
            'NEXUS_REPO_NAME': import_envs['SRC_NEXUS_REPO_NAME'],
            'NEXUS_ROOT_GROUP_ID': import_envs['SRC_NEXUS_ROOT_GROUP_ID'],
            'NEXUS_RW_PASSWORD': import_envs['SRC_NEXUS_RW_PASSWORD'],
            'NEXUS_RW_USERNAME': import_envs['SRC_NEXUS_RW_USERNAME'],

            # Default Env Vars
            'SYNC_MODE': 'TUTORIALS',
            'REGISTRY_HOST': import_envs[
                'SRC_REGISTRY_HOST'] if 'SRC_REGISTRY_HOST' in import_envs else 'cicd.ai4eu-dev.eu',

            # Env Vars to be set by User
            'GRAPHENE_TOKEN': import_envs['SRC_GRAPHENE_TOKEN'],
        }

        if 'SRC_SOLUTION_IDS' in import_envs:
            self.src_env_config['SOLUTION_IDS'] = import_envs['SRC_SOLUTION_IDS'].split(',')
        self.source_host = f'https://{self.src_env_config["GRAPHENE_HOST"]}'
        self.source_api_client = APIClient(self.src_env_config['GRAPHENE_USER'],
                                           self.src_env_config['GRAPHENE_PW'], self.source_host)

        if 'TEST_MODE' in import_envs:
            self.src_env_config['TEST_MODE'] = True
        else:
            self.src_env_config['TEST_MODE'] = False

        if 'SYNC_MODE' in import_envs and import_envs['SYNC_MODE']:
            self.src_env_config['SYNC_MODE'] = import_envs['SYNC_MODE']

        src_user_id = find_user_id(self.source_api_client, self.src_env_config['GRAPHENE_TOKEN'])
        src_catalog_id = find_catalog_id(self.source_api_client)
        self.src_env_config['GRAPHENE_USERID'] = src_user_id
        os.environ['SRC_GRAPHENE_USERID'] = src_user_id
        self.src_env_config['GRAPHENE_CATALOGID'] = src_catalog_id


class NotFoundError(Exception):
    pass


class NotActiveSolutionError(Exception):
    pass


def bordered(text):
    """
    This code is taken from:
    """
    lines = text.splitlines()
    width = max(len(s) for s in lines)
    res = ['┌' + '─' * width + '┐']
    for s in lines:
        res.append('│' + (s + ' ' * width)[:width] + '│')
    res.append('└' + '─' * width + '┘')
    return '\n'.join(res)


if __name__ == '__main__':
    env_vars_mgr = EnvVarsManager("C:\\EclipseGraphene\\dot_env_vars")
    target_env_vars = env_vars_mgr.target_env_config
