#!/bin/bash
echo "start kubernets install script";

export KUBE_CNI_VERSION=1.2.0

export CALICO_VERSION=v3.28.1


sudo swapoff -a
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab

sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

sudo modprobe overlay
sudo modprobe br_netfilter

sudo tee /etc/sysctl.d/kubernetes.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF


# Add Repositorys and install Packages

sudo sysctl --system

sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates

sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt update
sudo apt install -y containerd.io

containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml

sudo systemctl restart containerd
sudo systemctl enable containerd

curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.30/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.30/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

sudo apt update
sudo apt install -y --allow-downgrades kubernetes-cni kubectl kubelet kubeadm
sudo apt-mark hold kubelet kubeadm kubectl

sudo ufw disable


# Kubernetes Cluster Settings

tmp=~/$(uuidgen)

sudo kubeadm init --ignore-preflight-errors=all --pod-network-cidr=192.168.0.0/16 >>$tmp
cat $tmp
export k8s_joincmd=$(grep "kubeadm join" $tmp)
echo $k8s_joincmd >~/k8s_joincmd
rm $tmp

echo "[LOG] Cluster join command for manual use if needed: $k8s_joincmd";
echo "[LOG] Also saved in file ~/k8s_joincmd";

mkdir -p $HOME/.kube
sudo cp -f /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export KUBECONFIG=$HOME/.kube/config

tries=30
try=1
while ! kubectl taint node $HOSTNAME node.kubernetes.io/not-ready:NoSchedule- ; do
  echo "[LOG] Unable to remove not-ready taint from master; try $try of $tries";
  sleep 10
  try=$((try+1))
done


# Deploy pod network
echo "[LOG] Deploy calico as CNI";

kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/$CALICO_VERSION/manifests/tigera-operator.yaml
curl https://raw.githubusercontent.com/projectcalico/calico/$CALICO_VERSION/manifests/custom-resources.yaml -O
kubectl create -f custom-resources.yaml


# Deploy kubernetes dashboard
echo "[LOG] Deploy kubernetes dashboard";

# makes sure that the pods can be assigned by the sheduler (fixes status "pending ")
kubectl taint nodes --all node-role.kubernetes.io/control-plane-

wget https://raw.githubusercontent.com/kubernetes/dashboard/v2.6.1/aio/deploy/recommended.yaml
sed -i -e '1h;2,$H;$!d;g' -e 's~ports\:\n    - ~type\: NodePort\n  ports\:\n    - nodePort\: 32767\n      ~' recommended.yaml
sed -i '155,169d' recommended.yaml
kubectl create -f recommended.yaml

echo "[LOG] Enable default admin access to the kubernetes dashboard";

# per https://github.com/kubernetes/dashboard/wiki/Access-control#admin-privileges
cat <<EOF >dashboard-admin.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  labels:
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kube-system
EOF
  
kubectl create -f dashboard-admin.yaml


# Complete setup
echo "[KUBERNETES] Setup is complete!";

sudo apt-mark hold kubelet kubeadm kubectl kubernetes-cni
HOST_IP=$(/sbin/ip route get 8.8.8.8 | head -1 | sed 's/^.*src //' | awk '{print $1}')
echo "[LOG] The kubernetes dashboard is at https://$HOST_IP:32767"

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

# Integration of setup_helm script

function setup_helm() {
  trap 'fail' ERR
  echo "[HELM] Setup helm";
  wget https://get.helm.sh/helm-v3.12.3-linux-amd64.tar.gz
  tar -xvf helm-v3.12.3-linux-amd64.tar.gz
  sudo cp linux-amd64/helm /usr/local/bin/helm
}

setup_helm
echo "[HELM] Setup is complete";

# +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

